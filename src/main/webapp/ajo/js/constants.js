/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var APP_BASE_PATH = "/api/v1.0/ajo/";


var CURRENT = window.location.protocol + "//" + window.location.host;
var BASE_URL = CURRENT + APP_BASE_PATH;

var SIGN_UP_URL = BASE_URL + "user/create";

var LOGIN_URL = BASE_URL + "user/login";
var PERSONAL_INFO_URL = BASE_URL + "user/info/personal";
var INFO_URL = BASE_URL + "user/info";
var SAVINGS_URL = BASE_URL + "user/savings";
var USERS_SAVINGS_URL = BASE_URL + "users/savings";
var SAVINGS_CREATE_URL = BASE_URL + "user/savings/create";
var WITHDRAWALS_URL = BASE_URL + "user/withdrawals";
var USERS_WITHDRAWALS_URL = BASE_URL + "users/withdrawals";
var CONTRIBUTOR_WITHDRAWALS_URL = BASE_URL + "user/withdrawals/create";
var CONTRIBUTORS_URL = BASE_URL + "users";
var CONTRIBUTORS_DOWNLOAD_URL = BASE_URL + "users/download";
var SEARCH_CONTRIBUTORS_URL = BASE_URL + "users/search";
var CONTRIBUTORS_DETAIL_URL = BASE_URL + "user/info";
var DELETE_CONTRIBUTOR_URL = BASE_URL + "user/delete";
var LOANS_BY_TYPE_URL = BASE_URL + "user/loans/type";
var USER_ACTIVE_LOANS_BY_TYPE_URL = BASE_URL + "user/loans";
var LOANS_APPROVAL_URL = BASE_URL + "user/loans/approve";
var NEW_LOAN_URL = BASE_URL + "user/loans/create";
var REPAYMENT_URL = BASE_URL + "loan/repayment";
var ALL_REPAYMENT_URL = BASE_URL + "loan/repayments";
var AWAITING_LOANS_URL = BASE_URL + "user/loans/awaiting";
var ALL_LOANS_URL = BASE_URL + "user/loans/all";
var SUMMARY_URL = BASE_URL + "user/summary";
var ACTIVITIES_URL = BASE_URL + "activities/1";
var ALL_ACTIVITIES_URL = BASE_URL + "activities/all";
var NEW_CONTRIBUTOR_URL = BASE_URL + "user/create";
var WITHDRAWAL_APPROVAL_URL = BASE_URL + "withdrawals/approve";
var WITHDRAWAL_AWAITING_URL = BASE_URL + "user/withdrawals/awaiting";

var USER = "user-info";