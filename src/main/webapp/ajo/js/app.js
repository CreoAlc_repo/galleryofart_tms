/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var contributor = angular.module('contributor', ['ui.router', 'LocalStorageModule',
    'ui-notification', 'angular-loading-bar', 'angularModalService', 'ui.bootstrap',
    'ngFileSaver']);

contributor.config(function ($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;

    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'template/dashboard.html',
                controller: 'DashboardController'
            })
            .state('contributors', {
                url: '/contributors',
                templateUrl: 'template/contributors.html',
                controller: 'ContributorsController'
            })
            .state('activities', {
                url: '/activities',
                templateUrl: 'template/activities.html',
                controller: 'ActivitiesController'
            })
            .state('all-activities', {
                url: '/activities/all',
                templateUrl: 'template/all-activities.html',
                controller: 'AllActivitiesController'
            })
            .state('savings', {
                url: '/savings',
                templateUrl: 'template/savings.html',
                controller: 'SavingsController'
            })
            .state('withdrawals', {
                url: '/withdrawals',
                templateUrl: 'template/withdrawals.html',
                controller: 'WithdrawalsController'
            })
            .state('withdrawals-approval', {
                url: '/withdrawals/approval',
                templateUrl: 'template/withdrawalsapproval.html',
                controller: 'ApprovalsController'
            })
            .state('loans-regular', {
                url: '/loans/regular',
                templateUrl: 'template/loans-regular.html',
                controller: 'RegularLoansController'
            })
            .state('loans-special', {
                url: '/loans/special',
                templateUrl: 'template/loans-special.html',
                controller: 'SpecialLoansController'
            })
            .state('loans-commodity', {
                url: '/loans/commodity',
                templateUrl: 'template/loans-commodity.html',
                controller: 'CommodityLoansController'
            })
            .state('await-loans', {
                url: '/loans/awaits',
                templateUrl: 'template/awaiting-loans.html',
                controller: 'AwaitingLoansController'
            })
            .state('all-loans', {
                url: '/loans/all',
                templateUrl: 'template/all-loans.html',
                controller: 'AllLoansController'
            })
            .state('profile', {
                url: '/profile/{ippis}',
                templateUrl: 'template/profile-view.html',
                controller: 'ProfileController'
            })
            .state('update', {
                url: '/update/{ippis}',
                templateUrl: 'template/profile-update.html',
                controller: 'UpdateController'
            })
            .state('profile-update', {
                url: '/user/profile',
                templateUrl: 'template/user-profile.html',
                controller: 'UserProfileController'
            })
            .state('user-savings', {
                url: '/user/savings/{ippis}',
                templateUrl: 'template/user-savings.html',
                controller: 'UserSavingsController'
            })
            .state('user-withdrawals', {
                url: '/user/withdrawals/{ippis}',
                templateUrl: 'template/user-withdrawals.html',
                controller: 'UserWithdrawalController'
            })
            ;

    $httpProvider.interceptors.push('AjoAPI');
});

contributor.controller('DashboardController', function ($scope, $state, $log, $http,
        util) {

    $scope.popup = {
        opened: false
    };

    $scope.today = function () {
        $scope.dashboardYear = new Date();
    };
    $scope.today();

    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(2010, 5, 1),
        startingDay: 1,
        datepickerMode: 'year',
        minMode: 'year'
    };

    $scope.clear = function () {
        $scope.dashboardYear = null;
    };

    $scope.open = function () {
        $scope.popup.opened = true;
    };

    $scope.onYearSelected = function () {
        $scope.getSummary();
        $scope.getSavings(null, $scope.dashboardYear.getFullYear());
        $scope.getWithdrawals(null, $scope.dashboardYear.getFullYear())();
    };

    $scope.date = new Date();
    $scope.profile = {};
    $scope.savings = {};
    $scope.withdrawals = {};
    $scope.summary = [];

    $scope.getProfileInfo = function () {
        $http.get(PERSONAL_INFO_URL)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.profile = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.getSummary = function () {

        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.year = $scope.dashboardYear.getFullYear();

        $http.get(SUMMARY_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.summary = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.getSavings = function (id, year) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.year = year;
        $http.get(SAVINGS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.savings.length = 0;
                        $scope.savings = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.getWithdrawals = function (id, year) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.year = year;
        $http.get(WITHDRAWALS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.withdrawals.length = 0;
                        $scope.withdrawals = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.logout = function () {
        util.logout();
    };

    $scope.getSummary();
    $scope.getProfileInfo();
    $scope.getSavings(null, null);
    $scope.getWithdrawals(null, null);
});

contributor.controller('HeaderController', function ($scope, $state, $log, $http,
        util) {

    $scope.profile = function () {
        $state.transitionTo('profile-update');
    };

    $scope.logout = function () {
        util.logout();
    };

});

contributor.controller('SidebarController', function ($scope, $state, $log, $http,
        util) {

    $scope.profile = {};

    $scope.getProfileInfo = function () {
        $http.get(PERSONAL_INFO_URL)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.profile = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.getProfileInfo();
    $scope.isAdmin = function () {
        if (($scope.profile.role === 'ADMIN_1') ||
                ($scope.profile.role === 'ADMIN_2') ||
                ($scope.profile.role === 'ADMIN_3')) {
            return true;
        } else {
            return false;
        }
    };
    $scope.isAdmin1 = function () {
        if (($scope.profile.role === 'ADMIN_1')) {
            return true;
        } else {
            return false;
        }
    };
    $scope.isAdmin2 = function () {
        if (($scope.profile.role === 'ADMIN_2')) {
            return true;
        } else {
            return false;
        }
    };
    $scope.isAdmin3 = function () {
        if (($scope.profile.role === 'ADMIN_3')) {
            return true;
        } else {
            return false;
        }
    };
});

contributor.controller('SavingsController', function ($scope, $state, $log, $http,
        util, ModalService) {

    $scope.previouID = null;

    $scope.savings = {};

    $scope.getSavings = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(SAVINGS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.savings = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.savings)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getSavings($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.savings.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.savings)) {
            $scope.previouID = $scope.savings[0].id;
            if (!util.isNull($scope.savings[$scope.savings.length - 1])) {
                $scope.getSavings(
                        $scope.savings[$scope.savings.length - 1].id);
            }
        }

    };

    $scope.getSavings($scope.previouID);

});

contributor.controller('WithdrawalsController', function ($scope, $state, $log, $http,
        util, ModalService) {

    $scope.previouID = null;

    $scope.withdrawals = {};

    $scope.getWithdrawals = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(WITHDRAWALS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.withdrawals = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.withdrawals)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getWithdrawals($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.withdrawals.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.withdrawals)) {
            $scope.previouID = $scope.withdrawals[0].id;
            if (!util.isNull($scope.withdrawals[$scope.withdrawals.length - 1])) {
                $scope.getWithdrawals(
                        $scope.withdrawals[$scope.withdrawals.length - 1].id);
            }
        }

    };

    $scope.getWithdrawals($scope.previouID);

});

contributor.controller('RegularLoansController', function ($scope, $state, $log, $http,
        util) {

    $scope.previouID = null;

    $scope.loan_amount = "";
    $scope.loans = {};
    $scope.repayments = {};

    $scope.deductions = {};

    $scope.getLoans = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.type = 'REGULAR';
        $http.get(LOANS_BY_TYPE_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.loans = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.loans)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getLoans($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.loans.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.loans)) {
            $scope.previouID = $scope.loans[0].id;
            if (!util.isNull($scope.loans[$scope.loans.length - 1])) {
                $scope.getLoans(
                        $scope.loans[$scope.loans.length - 1].id);
            }
        }

    };

    $scope.showDeductions = function (loan) {

        $scope.loan_amount = loan.amount;
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = loan.id;
        $http.get(ALL_REPAYMENT_URL, $scope.config)
                .then(
                        function (success) {
                            if (!util.isNull(success.data.data)) {
                                $scope.repayments = success.data.data;
                            }
                        },
                        function (error) {
                        });
    };

    $scope.getLoans($scope.previouID);
});

contributor.controller('SpecialLoansController', function ($scope, $state, $log, $http,
        util) {

    $scope.previouID = null;

    $scope.loans = {};

    $scope.getLoans = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.type = 'SPECIAL';
        $http.get(LOANS_BY_TYPE_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.loans = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.loans)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getLoans($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.loans.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.loans)) {
            $scope.previouID = $scope.loans[0].id;
            if (!util.isNull($scope.loans[$scope.loans.length - 1])) {
                $scope.getLoans(
                        $scope.loans[$scope.loans.length - 1].id);
            }
        }

    };

    $scope.getLoans($scope.previouID);
});

contributor.controller('CommodityLoansController', function ($scope, $state, $log, $http,
        util) {

    $scope.previouID = null;

    $scope.loans = {};

    $scope.getLoans = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.type = 'COMMODITY';
        $http.get(LOANS_BY_TYPE_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.loans = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.loans)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getLoans($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.loans.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.loans)) {
            $scope.previouID = $scope.loans[0].id;
            if (!util.isNull($scope.loans[$scope.loans.length - 1])) {
                $scope.getLoans(
                        $scope.loans[$scope.loans.length - 1].id);
            }
        }

    };

    $scope.getLoans($scope.previouID);
});

contributor.controller('AwaitingLoansController', function ($scope, $state, $log, $http,
        util, notify, $window, $timeout) {

    $scope.previouID = null;

    $scope.loans = {};

    $scope.getLoans = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(AWAITING_LOANS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.loans = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.loans)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getLoans($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.loans.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.loans)) {
            $scope.previouID = $scope.loans[0].id;
            if (!util.isNull($scope.loans[$scope.loans.length - 1])) {
                $scope.getLoans(
                        $scope.loans[$scope.loans.length - 1].id);
            }
        }

    };

    $scope.approve = function (id) {

        $scope.approval = {};
        $scope.approval.id = id;
        $scope.approval.approval = "APPROVED";

        $scope.config = {};
        $http.post(LOANS_APPROVAL_URL, $scope.approval, $scope.config)
                .then(function (success)
                {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);
                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                    } else {
                        notify.warning(success.data.status.description);
                    }
                }, function (error) {
                });
    };

    $scope.decline = function (id) {


        $scope.approval = {};
        $scope.approval.id = id;
        $scope.approval.approval = "DECLINED";

        $scope.config = {};

        $http.post(LOANS_APPROVAL_URL, $scope.approval, $scope.config)
                .then(function (success) {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);
                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                    } else {
                        notify.warning(success.data.status.description);
                    }
                }, function (error) {
                });
    };


    $scope.getLoans($scope.previouID);

});

contributor.controller('AllLoansController', function ($scope, $state, $log, $http,
        util, ModalService) {

    $scope.previouID = null;

    $scope.loan_amount = "";
    $scope.loans = {};
    $scope.repayments = {};

    $scope.showLoanModal = function (cName, ippis, loanid) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/repayment-modal.html",
            controller: "LoanRepaymentController",
            inputs: {
                name: cName,
                ippis: ippis,
                id: loanid
            }
        }
        ).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.showNewLoanModal = function (cName, ippis) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/new-loan-modal.html",
            controller: "NewLoanController",
            inputs: {
                name: cName,
                ippis: ippis
            }
        }
        ).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.getLoans = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(ALL_LOANS_URL, $scope.config)
                .then(function (success)
                {
                    if (!util.isNull(success.data.data)) {
                        $scope.loans = success.data.data;
                    }
                },
                        function (error) {
                        });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.loans)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getLoans($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.loans.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.loans)) {
            $scope.previouID = $scope.loans[0].id;
            if (!util.isNull($scope.loans[$scope.loans.length - 1])) {
                $scope.getLoans(
                        $scope.loans[$scope.loans.length - 1].id);
            }
        }

    };

    $scope.showDeductions = function (loan) {

        $scope.loan_amount = loan.amount;
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = loan.id;
        $http.get(ALL_REPAYMENT_URL, $scope.config)
                .then(
                        function (success) {
                            if (!util.isNull(success.data.data)) {
                                $scope.repayments = success.data.data;
                            }
                        },
                        function (error) {
                        });
    };

    $scope.getLoans($scope.previouID);

});

contributor.controller('ActivitiesController', function ($scope, $state, $log, $http,
        util, ModalService) {

    $scope.previouID = null;

    $scope.activities = {};

    $scope.getActivities = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(ACTIVITIES_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.activities = success.data.data;
                    }
                }, function (error) {
                });
    };


    $scope.previous = function () {

        if (!util.isNull($scope.activities)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getActivities($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.activities.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.activities)) {
            $scope.previouID = $scope.activities[0].id;
            if (!util.isNull($scope.activities[$scope.activities.length - 1])) {
                $scope.getActivities(
                        $scope.activities[$scope.activities.length - 1].id);
            }
        }

    };

    $scope.getActivities($scope.previouID);

});

contributor.controller('AllActivitiesController', function ($scope, $state, $log, $http,
        util, ModalService) {

    $scope.previouID = null;

    $scope.activities = {};

    $scope.getActivities = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(ALL_ACTIVITIES_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.activities = success.data.data;
                    }
                }, function (error) {
                });
    };


    $scope.previous = function () {

        if (!util.isNull($scope.activities)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getActivities($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.activities.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.activities)) {
            $scope.previouID = $scope.activities[0].id;
            if (!util.isNull($scope.activities[$scope.activities.length - 1])) {
                $scope.getActivities(
                        $scope.activities[$scope.activities.length - 1].id);
            }
        }

    };

    $scope.getActivities($scope.previouID);
});

contributor.controller('ContributorsController', function ($scope, $state, $log, $http, $window,
        util, ModalService, FileSaver, Blob, notify) {

    $scope.month = "";
    $scope.search = "";
    $scope.previouID = null;

    $scope.contributors = {};

    $scope.searchContributors = function () {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.ippis = $scope.search;
        $http.get(SEARCH_CONTRIBUTORS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        if (!util.isNull(success.data.data)) {
                            if (success.data.data.length > 0) {
                                $scope.contributors = success.data.data;
                            }
                        }
                    }
                }, function (error) {
                });
    };

    $scope.getContributors = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(CONTRIBUTORS_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.contributors = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.downloadContributors = function (month) {
        if (util.isNull(month) || (month.length === 0)) {
            notify.warning("Invalid month selection");
        } else {
            $scope.config = {};
            $scope.config.params = {};
            $scope.config.params.month = month;
            $scope.config.responseType = 'blob';

            $http.get(CONTRIBUTORS_DOWNLOAD_URL, $scope.config)
                    .then(function (success)
                    {
                        var blob = new Blob([success.data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                        FileSaver.saveAs(blob, 'contributors-' + month + '.csv');
                    }, function (error) {
                        notify.error(error);
                    });
        }
    };
//
//    $scope.downloadContributors = function () {
//
//        $scope.config = {};
//        $scope.config.responseType = 'blob';
//
//        $http.get(CONTRIBUTORS_DOWNLOAD_URL, $scope.config)
//                .then(function (success)
//                {
//                    var blob = new Blob([success.data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
//                    FileSaver.saveAs(blob, 'contributors' + '.csv');
//                }
//                , function (error) {
//                    $log.warn(error);
//                });
//    };

    $scope.previous = function () {

        if (!util.isNull($scope.contributors)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getContributors($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.contributors.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.contributors)) {
            $scope.previouID = $scope.contributors[0].id;
            if (!util.isNull($scope.contributors[$scope.contributors.length - 1])) {
                $scope.getContributors(
                        $scope.contributors[$scope.contributors.length - 1].id);
            }
        }

    };

    $scope.viewDetail = function (ippis) {
        $scope.param = {};
        $scope.param.ippis = ippis;
        $state.transitionTo('profile', $scope.param);
    };

    $scope.viewSavings = function (ippis) {
        $scope.param = {};
        $scope.param.ippis = ippis;
        $state.transitionTo('user-savings', $scope.param);
    };

    $scope.viewWithdrawals = function (ippis) {
        $scope.param = {};
        $scope.param.ippis = ippis;
        $state.transitionTo('user-withdrawals', $scope.param);
    };

    $scope.updateDetail = function (ippis) {
        $scope.param = {};
        $scope.param.ippis = ippis;
        $state.transitionTo('update', $scope.param);
    };

    $scope.showRoleModal = function (cName, ippis) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/role-modal.html",
            controller: "RoleController",
            inputs: {
                name: cName,
                ippis: ippis
            }
        }).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $state.transitionTo('contributors');
            });
        });

    };

    $scope.showCreditModal = function (cName, ippis) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/credit-modal.html",
            controller: "CreditController",
            inputs: {
                name: cName,
                ippis: ippis
            }
        }).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.showLoanModal = function (cName, ippis) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/loan-modal.html",
            controller: "NewLoanController",
            inputs: {
                name: cName,
                ippis: ippis
            }
        }
        ).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.showDebitModal = function (cName, ippis) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/debit-modal.html",
            controller: "DebitController",
            inputs: {
                name: cName,
                ippis: ippis
            }
        }).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.showDeleteModal = function (cName, ippis) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/delete-modal.html",
            controller: "DeleteController",
            inputs: {
                name: cName,
                ippis: ippis
            }
        }).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.showNewContributorModal = function () {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/contributor-modal.html",
            controller: "NewAcctController"
        }).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.profile = {};

    $scope.getProfileInfo = function () {
        $http.get(PERSONAL_INFO_URL)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.profile = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.isAdmin = function () {
        if (($scope.profile.role === 'ADMIN_1') ||
                ($scope.profile.role === 'ADMIN_2') ||
                ($scope.profile.role === 'ADMIN_3')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin1And2 = function () {
        if (($scope.profile.role === 'ADMIN_1') ||
                ($scope.profile.role === 'ADMIN_2')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin1 = function () {
        if (($scope.profile.role === 'ADMIN_1')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin2 = function () {
        if (($scope.profile.role === 'ADMIN_2')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin3 = function () {
        if (($scope.profile.role === 'ADMIN_3')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.getProfileInfo();

    $scope.getContributors($scope.previouID);

});

contributor.controller('ApprovalsController', function ($scope, $state, $log, $http,
        util, notify, $window, $timeout) {

    $scope.search = "";
    $scope.previouID = null;

    $scope.withdrawals = {};

    $scope.getWithdrawals = function (id) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $http.get(WITHDRAWAL_AWAITING_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.withdrawals = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.previous = function () {

        if (!util.isNull($scope.withdrawals)) {
            if (!util.isNull($scope.previouID)) {
                $scope.getWithdrawals($scope.previouID);
                $scope.previouID = $scope.previouID + $scope.withdrawals.length;
            }
        }

    };

    $scope.next = function () {

        if (!util.isNull($scope.withdrawals)) {
            $scope.previouID = $scope.withdrawals[0].id;
            if (!util.isNull($scope.withdrawals[$scope.withdrawals.length - 1])) {
                $scope.getWithdrawals(
                        $scope.withdrawals[$scope.withdrawals.length - 1].id);
            }
        }

    };

    $scope.approve = function (id) {

        $scope.approval = {};
        $scope.approval.id = id;
        $scope.approval.status = "APPROVED";

        $scope.config = {};
        $http.post(WITHDRAWAL_APPROVAL_URL, $scope.approval, $scope.config)
                .then(function (success)
                {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);
                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                    } else {
                        notify.warning(success.data.status.description);
                    }
                }, function (error) {
                });
    };

    $scope.decline = function (id) {
        $scope.approval = {};
        $scope.approval.id = id;
        $scope.approval.status = "DECLINED";

        $scope.config = {};

        $http.post(WITHDRAWAL_APPROVAL_URL, $scope.approval, $scope.config)
                .then(function (success) {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);
                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                    } else {
                        notify.warning(success.data.status.description);
                    }
                }, function (error) {
                });
    };

    $scope.profile = {};

    $scope.getProfileInfo = function () {
        $http.get(PERSONAL_INFO_URL)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.profile = success.data.data;
                    }
                }, function (error) {
                });
    };

    $scope.isAdmin = function () {
        if (($scope.profile.role === 'ADMIN_1') ||
                ($scope.profile.role === 'ADMIN_2') ||
                ($scope.profile.role === 'ADMIN_3')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin1And2 = function () {
        if (($scope.profile.role === 'ADMIN_1') ||
                ($scope.profile.role === 'ADMIN_2')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin1 = function () {
        if (($scope.profile.role === 'ADMIN_1')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin2 = function () {
        if (($scope.profile.role === 'ADMIN_2')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isAdmin3 = function () {
        if (($scope.profile.role === 'ADMIN_3')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.getProfileInfo();

    $scope.getWithdrawals($scope.previouID);

});

contributor.controller('ProfileController', function ($scope, $state, $log, $http,
        util, ModalService, $stateParams) {

    $scope.profile = {};
    $scope.loans = {};

    $scope.getProfileInfo = function (ippis) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.ippis = ippis;
        $http.get(CONTRIBUTORS_DETAIL_URL, $scope.config)
                .then(
                        function (success) {
                            if (!util.isNull(success.data.data)) {
                                $scope.profile = success.data.data;
                            }
                        },
                        function (error) {
                        });
    };

    $scope.loadLoans = function (ippis) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.ippis = ippis;
        $http.get(USER_ACTIVE_LOANS_BY_TYPE_URL, $scope.config)
                .then(
                        function (success) {
                            if (!util.isNull(success.data.data)) {
                                $scope.loans = success.data.data;
                            }
                        },
                        function (error) {
                        });
    };

    $scope.showLoanModal = function (cName, ippis, loanid) {

        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: "template/repayment-modal.html",
            controller: "LoanRepaymentController",
            inputs: {
                name: cName,
                ippis: ippis,
                id: loanid
            }
        }
        ).then(function (modal) {
            // The modal object has the element built, if this is a bootstrap modal
            // you can call 'modal' to show it, if it's a custom modal just show or hide
            // it as you need to.
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = result ? "You said Yes" : "You said No";
            });
        });

    };

    $scope.getProfileInfo($stateParams.ippis);
    $scope.loadLoans($stateParams.ippis);
});

contributor.controller('UpdateController', function ($scope, $state, $log, $http,
        util, $stateParams, notify, $timeout) {

    $scope.user = {};
    $scope.user.ippis = $stateParams.ippis;

    $scope.profile = {};

    $scope.getProfileInfo = function (ippis) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.ippis = ippis;
        $http.get(CONTRIBUTORS_DETAIL_URL, $scope.config)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.profile = success.data.data;
                        $scope.user.name = $scope.profile.name;
                        $scope.user.email = $scope.profile.email;
                        $scope.user.phone = $scope.profile.phoneNumber;
                        $scope.user.amount = $scope.profile.contributionAmount;
                    }
                }, function (error) {
                });
    };

    $scope.updateInfo = function () {
        $http.post(CONTRIBUTORS_DETAIL_URL, $scope.user)
                .then(function (success) {
                    notify.success("User information updated");
                    $timeout(function () {
                        $state.transitionTo('contributors');
                    }, 3000);
                }, function (error) {
                });
    };

    $scope.getProfileInfo($stateParams.ippis);
});

contributor.controller('UserProfileController', function ($scope, $state, $log, $http,
        util, $stateParams, notify, $timeout) {

    $scope.user = {};
    $scope.user.ippis = $stateParams.ippis;

    $scope.profile = {};

    $scope.getProfileInfo = function () {
        $http.get(PERSONAL_INFO_URL)
                .then(function (success) {
                    if (!util.isNull(success.data.data)) {
                        $scope.profile = success.data.data;
                        $scope.user.ippis = $scope.profile.ippisNumber;
                        $scope.user.phone = $scope.profile.phoneNumber;
                        $scope.user.email = $scope.profile.email;
                        $scope.user.name = $scope.profile.name;
                    }
                }, function (error) {
                });
    };


    $scope.updateInfo = function () {
        $log.info($scope.user);
        $http.post(CONTRIBUTORS_DETAIL_URL, $scope.user)
                .then(function (success) {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);
                        $timeout(function () {
                            $state.transitionTo('dashboard');
                        }, 3000);
                    } else {
                        notify.warning(success.data.status.description);
                    }
                }, function (error) {
                });
    };

    $scope.getProfileInfo();
});

contributor.controller('RoleController', function ($scope, $state, $log, $http,
        util, $stateParams, notify, $timeout, $window, name, ippis, close) {

    $scope.user = {};
    $scope.user.ippis = ippis;

    $scope.updateInfo = function () {
        $http.post(CONTRIBUTORS_DETAIL_URL, $scope.user)
                .then(function (success) {
                    notify.success("Role assinged successfully");

                    $timeout(function () {
                        $window.location.reload();
                    }, 2000);
                    $scope.dismissModal();
                }, function (error) {
                });
    };

    $scope.name = name;

    $scope.assignRole = function () {
        $scope.updateInfo();
    };

    $scope.dismissModal = function (result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };
});

contributor.controller('CreditController', function ($scope, $state, $log, $http,
        util, notify, $window, $timeout, name, ippis, close) {

    $scope.name = name;
    $scope.user = {};
    $scope.user.name = ippis;

    $scope.savings = function () {

        $scope.config = {};
        $scope.config.headers = {
            'Content-Type': 'application/json'
        };
        var call = $http.post(SAVINGS_CREATE_URL, $scope.user, $scope.config)
                .then(function (success) {
                    notify.success(success.data.status.description);

                    $timeout(function () {
                        $window.location.reload();
                    }, 2000);
                    $scope.dismissModal();
                }, function (error) {
                    $log.warn(error);
                });
    };

    $scope.dismissModal = function (result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };
});

contributor.controller('NewLoanController', function ($scope, $state, $log, $http,
        util, notify, $window, $timeout, name, ippis, close) {

    $scope.name = name;
    $scope.ippis = ippis;
    $scope.user = {};
    $scope.user.ippis = ippis;

    $scope.requestForLoan = function () {
        $http.post(NEW_LOAN_URL, $scope.user)
                .then(function (success) {
                    if (success.data.status.code === 0) {
                        notify.success(success.data.status.description);
                    } else {
                        notify.warning(success.data.status.description);
                    }
                    $timeout(function () {
                        $window.location.reload();
                    }, 2000);
                    $scope.dismissModal();
                }
                , function (error) {
                    $log.warn(error);
                });
    };
    $scope.dismissModal = function (result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };
});

contributor.controller('LoanRepaymentController', function ($scope, $state, $log, $http,
        util, notify, $window, $timeout, name, ippis, id, close) {

    $scope.name = name;
    $scope.ippis = ippis;
    $scope.id = id;
    $scope.user = {};
    $scope.user.id = id;
    $scope.user.name = name;
    $scope.user.ippis = ippis;


    $scope.updateRepayment = function () {
        $http.post(REPAYMENT_URL, $scope.user)
                .then(function (success) {
                    notify.success(success.data.status.description);
                    $timeout(function () {
                        $window.location.reload();
                    }, 2000);
                    $scope.dismissModal();
                }
                , function (error) {
                    $log.warn(error);
                });
    };
    $scope.dismissModal = function (result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };
});

contributor.controller('DebitController', function ($scope, $state, $log, $http,
        util, notify, $timeout, $window, name, ippis, close) {


    $scope.withdrawal = {};
    $scope.name = name;
    $scope.withdrawal.name = ippis;

    $scope.createWithdrawal = function () {
        $http.post(CONTRIBUTOR_WITHDRAWALS_URL, $scope.withdrawal)
                .then(function (success) {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);

                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                        $scope.dismissModal();
                    } else {
                        notify.warning(success.data.status.description);
                    }

                }, function (error) {
                });
    };

    $scope.dismissModal = function (result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };
});

contributor.controller('DeleteController', function ($scope, $state, $log, $http,
        notify, $timeout, $window, name, ippis, close) {

    $scope.name = name;
    $scope.ippis = ippis;

    $scope.deleteContributor = function () {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.ippis = ippis;

        $http.get(DELETE_CONTRIBUTOR_URL, $scope.config)
                .then(function (success)
                {
                    $log.warn(success);
                    if (success.data.status.code === 0) {
                        notify.success(success.data.status.description);

                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                        $scope.dismissModal();
                    } else {
                        notify.warning(success.status.description);
                    }

                }, function (error) {
                });
    };

    $scope.dismissModal = function (result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };
});

contributor.controller('NewAcctController', function ($scope, $state, $log, $http,
        $window, util, notify, $timeout, close) {

    $scope.lastname = "";
    $scope.firstname = "";
    $scope.user = {};


    $scope.createContributor = function () {
        $scope.user.name = ($scope.lastname + " " + $scope.firstname).toUpperCase();
        $scope.user.role = 'NORMAL';

        $http.post(NEW_CONTRIBUTOR_URL, $scope.user)
                .then(function (success)
                {
                    if (util.equals(success.data.status.code, 0)) {
                        notify.success(success.data.status.description);
                        $timeout(function () {
                            $window.location.reload();
                        }, 2000);
                        $scope.dismissModal();
                    } else {
                        notify.warning(success.data.status.description);
                    }
                },
                        function (error) {
                        });
    };

    $scope.dismissModal = function (result) {
        close(result, 200);
    };
});

contributor.controller('UserSavingsController', function ($scope, $state, $log, $http,
        $window, util, notify, $timeout, $stateParams) {

    $scope.savings = [];
    $scope.getSavings = function (id, ippis) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.ippis = ippis;
        $scope.config.params.year = null;
        $log.warn(ippis);
        $http.get(USERS_SAVINGS_URL, $scope.config)
                .then(function (success)
                {
                    if (!util.isNull(success.data.data)) {
                        $scope.savings = success.data.data;
                    }
                }
                , function (error) {
                });
    };
    $scope.getSavings(null, $stateParams.ippis);

});

contributor.controller('UserWithdrawalController', function ($scope, $state, $log, $http,
        $window, util, notify, $timeout, $stateParams) {

    $scope.withdrawals = {};

    $scope.getWithdrawals = function (id, ippis) {
        $scope.config = {};
        $scope.config.params = {};
        $scope.config.params.id = id;
        $scope.config.params.ippis = ippis;
        $scope.config.params.year = null;
        $http.get(USERS_WITHDRAWALS_URL, $scope.config)
                .then(function (success)
                {
                    if (!util.isNull(success.data.data)) {
                        $scope.withdrawals = success.data.data;
                    }
                }
                , function (error) {
                });
    };

    $scope.getWithdrawals(null, $stateParams.ippis);
});

contributor.service('util', function (localStorageService, $window) {

    this.isNull = function (value) {
        return angular.isUndefined(value) || (value === null);
    };
    this.equals = function (o1, o2) {
        return angular.equals(o1, o2);
    };
    this.isLoggedIn = function () {
        var user = localStorageService.get(USER);
        if (isNull(user)) {
            return false;
        } else {
            if (isNull(user.auth)) {
                return false;
            } else {
                return true;
            }
        }

    };
    this.getUser = function () {
        return localStorageService.get(USER);
    };

    this.getToken = function () {
        var user = this.getUser();
        if (this.isNull(user)) {
            return null;
        } else {
            return user.token;
        }
    };

    this.getRole = function () {
        var auth = this.getAuth();
        if (this.isNull(auth)) {
            return null;
        } else {
            return auth.v.role;
        }
    };

    this.isAdmin1 = function () {
        return this.equals(this.getRole(), 'ADMIN');
    };
    this.isAdmin2 = function () {
        return this.equals(this.getRole(), 'AGENT');
    };
    this.isAdmin3 = function () {
        return this.equals(this.getRole(), 'CLIENT');
    };
    this.logout = function () {
        localStorageService.clearAll();
        $window.location.href = 'login.html';
    };
});

contributor.service('AjoAPI', function ($rootScope, util, $log) {
    var service = this;
    service.request = function (config) {
        config.headers['Content-Type'] = 'application/json';
        var token = util.getToken();
        if (!util.isNull(token)) {
            config.headers.authorization = 'Bearer ' + token;
        }

        return config;
    };
    service.response = function (response) {

        if (response.config.url.includes('api/v1.0/ajo')) {
            var contenttype = response.headers('Content-Type');
            if (contenttype === 'application/vnd.ms-excel') {

            } else if (contenttype === 'application/json') {
                if (response.status === 200) {
                    if (response.data.status.code === 212) {
                        util.logout();
                    }
                }
            }

        }
        return response;
    };
});

contributor.service('notify', function (Notification) {
    var service = this;
    notification = {};
    notification.message = '';
    notification.title = 'Notification';
    notification.delay = 3000;
    service.info = function (message) {
        notification.message = message;
        return Notification.info(notification);
    };
    service.success = function (message) {
        notification.message = message;
        return Notification.success(notification);
    };
    service.warning = function (message) {
        notification.message = message;
        return Notification.warning(notification);
    };
    service.error = function (message) {
        notification.message = message;
        return Notification.error(notification);
    };
    service.clearAll = function () {
        return Notification.clearAll;
    };
});

contributor.directive('formatCurrency', function ($filter) {
    return {
        require: 'ngModel',
        scope: {
            format: "="
        },
        link: function (scope, element, attrs, ngModelController) {
            ngModelController.$parsers.push(function (data) {
                //convert data from view format to model format
                return $filter('currency')(data, '₦', 2);
            });

            ngModelController.$formatters.push(function (data) {
                //convert data from model format to view format
                return $filter('currency')(data, '₦', 2);
            });
        }
    };
});

var login = angular.module('login', ['ui.router', 'LocalStorageModule'
            , 'ui-notification', 'angular-loading-bar']);
login.controller('LoginController', function ($scope, $log, $window, $http,
        $timeout, Notification, localStorageService) {


    $scope.notification = {};
    $scope.notification.message = '';
    $scope.notification.title = 'Notification';
    $scope.notification.delay = 3000;

    $scope.user = {};

    $scope.config = {};
    $scope.config.headers = {
        'Content-Type': 'application/json'
    };


    $scope.login = function () {

        $http.post(LOGIN_URL, $scope.user, $scope.config)
                .then(function (success) {
                    if (success.data.status.code === 0) {
                        $scope.notification.message = success.data.status.description;
                        Notification.success($scope.notification);
                        var user = {};
                        user.token = success.data.data;
                        localStorageService.set(USER, user);
                        $timeout(function () {
                            $window.location.href = 'index.html';
                        }, 1000);
                    } else if (success.data.status.code === 310) {
                        $scope.notification.message = success.data.status.description;
                        Notification.error($scope.notification);
                    } else if (success.data.status.code === 204) {
                        $scope.notification.message = success.data.status.description;
                        Notification.error($scope.notification);
                    } else {
                        $scope.notification.message = success.data.status.description;
                        Notification.warning($scope.notification);
                    }
                }, function (error) {
                    $scope.notification.message = 'Error connecting to server';
                    Notification.error($scope.notification);
                });
    };

}); 