/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import com.silverbullet.clients.ajo.entities.Loan;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 */
public class ApproveLoan implements Serializable {

    private static final long serialVersionUID = -4818673990757742926L;

    private Loan.Approval approval;
    private Long id;

    public ApproveLoan() {
    }

    public ApproveLoan(Loan.Approval approval, Long id) {
        this.approval = approval;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loan.Approval getApproval() {
        return approval;
    }

    public void setApproval(Loan.Approval approval) {
        this.approval = approval;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.approval);
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApproveLoan other = (ApproveLoan) obj;
        if (this.approval != other.approval) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
