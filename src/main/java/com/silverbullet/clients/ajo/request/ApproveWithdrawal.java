/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import com.silverbullet.clients.ajo.entities.Withdrawal;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 */
public class ApproveWithdrawal implements Serializable {

    private static final long serialVersionUID = -4818673990757742926L;

    private Withdrawal.Status status;
    private Long id;

    public ApproveWithdrawal() {
    }

    public ApproveWithdrawal(Withdrawal.Status status, Long id) {
        this.status = status;
        this.id = id;
    }

    public Withdrawal.Status getStatus() {
        return status;
    }

    public void setStatus(Withdrawal.Status status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.status);
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApproveWithdrawal other = (ApproveWithdrawal) obj;
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
