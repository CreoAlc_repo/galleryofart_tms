/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class StringBean implements Serializable {

    private static final long serialVersionUID = -5106877012997119745L;

    private String name;

    private String value;

    public StringBean() {
    }

    public StringBean(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
