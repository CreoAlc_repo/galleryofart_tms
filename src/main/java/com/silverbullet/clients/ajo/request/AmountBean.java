/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class AmountBean implements Serializable {

    private static final long serialVersionUID = 8476585504583861694L;

    private String name;

    private Double amount;

    public AmountBean() {
    }

    public AmountBean(String name, Double amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
