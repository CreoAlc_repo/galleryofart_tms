/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import com.silverbullet.clients.ajo.entities.User;
import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class UserBean implements Serializable {

    private static final long serialVersionUID = -1125343191180499896L;

    private String ippis;

    private String name;

    private String phone;

    private String email;
    
    private String password;
    
    private String cpassword;

    private User.Role role;

    private Double amount;

    public UserBean() {
    }

    public UserBean(String ippis, String name, String phone, String email,
            Double amount) {
        this.ippis = ippis;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.amount = amount;
    }

    public String getIppis() {
        return ippis;
    }

    public void setIppis(String ippis) {
        this.ippis = ippis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User.Role getRole() {
        return role;
    }

    public void setRole(User.Role role) {
        this.role = role;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }

    @Override
    public String toString() {
        return "UserBean{" + "ippis=" + ippis + ", name=" + name + ", phone=" + phone + ", email=" + email + ", role=" + role + ", amount=" + amount + '}';
    }

}
