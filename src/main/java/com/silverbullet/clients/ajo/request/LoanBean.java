/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import com.silverbullet.clients.ajo.entities.Loan;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 */
public class LoanBean implements Serializable {

    private static final long serialVersionUID = -1143978654312826120L;

    private String ippis;
    private Double amount;
    private Double repayment;
    private Loan.Type type;

    public LoanBean() {
    }

    public LoanBean(String ippis, Double amount, Double repayment, Loan.Type type) {
        this.ippis = ippis;
        this.amount = amount;
        this.repayment = repayment;
        this.type = type;
    }

    public String getIppis() {
        return ippis;
    }

    public void setIppis(String ippis) {
        this.ippis = ippis;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getRepayment() {
        return repayment;
    }

    public void setRepayment(Double repayment) {
        this.repayment = repayment;
    }

    public Loan.Type getType() {
        return type;
    }

    public void setType(Loan.Type type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.amount);
        hash = 67 * hash + Objects.hashCode(this.repayment);
        hash = 67 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LoanBean other = (LoanBean) obj;
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.repayment, other.repayment)) {
            return false;
        }
        return this.type == other.type;
    }

}
