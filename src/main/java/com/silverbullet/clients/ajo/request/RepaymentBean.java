/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.request;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 */
public class RepaymentBean implements Serializable{

    private static final long serialVersionUID = -5207479133632260714L;
    
    private Long id;
    
    private String name;
    
    private String ippis;
    
    private Double repayment;

    public RepaymentBean() {
    }

    public RepaymentBean(Long id, String name, String ippis, Double repayment) {
        this.id = id;
        this.name = name;
        this.ippis = ippis;
        this.repayment = repayment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIppis() {
        return ippis;
    }

    public void setIppis(String ippis) {
        this.ippis = ippis;
    }

    public Double getRepayment() {
        return repayment;
    }

    public void setRepayment(Double repayment) {
        this.repayment = repayment;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.ippis);
        hash = 47 * hash + Objects.hashCode(this.repayment);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RepaymentBean other = (RepaymentBean) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.ippis, other.ippis)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return Objects.equals(this.repayment, other.repayment);
    }
    
}
