/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo;

import com.silverbullet.clients.ajo.entities.Loan;
import com.silverbullet.clients.ajo.entities.User;
import com.silverbullet.clients.ajo.request.AmountBean;
import com.silverbullet.clients.ajo.request.ApproveLoan;
import com.silverbullet.clients.ajo.request.ApproveWithdrawal;
import com.silverbullet.clients.ajo.request.LoanBean;
import com.silverbullet.clients.ajo.request.RepaymentBean;
import com.silverbullet.clients.ajo.request.StringBean;
import com.silverbullet.clients.ajo.request.UserBean;
import com.silverbullet.gaora.annotations.Secure;
import com.silverbullet.gaora.request.LoginBean;
import com.silverbullet.gaora.responses.BaseResponse;
import java.io.File;
import java.io.IOException;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 *
 * @author prodigy4440
 */
@Path("ajo")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class Ajo {

  @EJB
  private UserManager userManager;

  @EJB
  private Service service;

  @Secure
  @Path("user/create")
  @POST
  public Response createUser(@HeaderParam("Authorization") String bearer,
      UserBean userBean) {
    return Response.ok(service.createUser(userManager.getUser(bearer),
        userBean)).build();
  }

  @Path("user/login")
  @POST
  public Response loginUser(LoginBean loginBean) {
    return Response.ok(service.loginUser(loginBean)).build();
  }

  @Secure
  @Path("user/info/personal")
  @GET
  public Response getPersonalInfo(@HeaderParam("Authorization") String bearer) {

    User user = userManager.getUser(bearer);

    return Response.ok(service.getInfo(userManager.getUser(bearer),
        user.getIppisNumber())).build();
  }

  @Secure
  @Path("user/info")
  @GET
  public Response getInfo(@HeaderParam("Authorization") String bearer,
      @QueryParam("ippis") String ippis) {
    return Response.ok(service.getInfo(userManager.getUser(bearer), ippis)).build();
  }

  @Secure
  @Path("user/password/change")
  @POST
  public Response changePassword(@HeaderParam("Authorization") String bearer,
      StringBean stringBean) {
    return Response.ok(service.changePassword(userManager.getUser(bearer),
        stringBean.getName(), stringBean.getValue())).build();
  }

  @Secure
  @Path("user/password/reset")
  @POST
  public Response resetPassword(@HeaderParam("Authorization") String bearer,
      StringBean stringBean) {
    return Response.ok(service.resetPassword(userManager.getUser(bearer),
        stringBean.getName(), stringBean.getValue())).build();
  }

  @Secure
  @Path("user/savings/create")
  @POST
  public Response createSavings(@HeaderParam("Authorization") String bearer,
      AmountBean amountBean) {
    return Response.ok(service.createSavings(userManager.getUser(bearer),
        amountBean.getName(), amountBean.getAmount())).build();
  }

  @Secure
  @Path("user/savings")
  @GET
  public Response getSavings(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID, @QueryParam("year") String year) {
    return Response.ok(service.getSavings(userManager.getUser(bearer), lastID, year))
        .build();
  }

  @Secure
  @Path("users/savings")
  @GET
  public Response getSavings(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID, @QueryParam("ippis") String ippis,
      @QueryParam("year") String year) {
    return Response.ok(service.getSavings(userManager.getUser(bearer), ippis, lastID, year))
        .build();
  }

  @Secure
  @Path("user/withdrawals")
  @GET
  public Response getWithdrawals(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID, @QueryParam("year") String year) {
    return Response.ok(service.getWithdrawals(userManager.getUser(bearer),
        lastID, year))
        .build();
  }

  @Secure
  @Path("users/withdrawals")
  @GET
  public Response getWithdrawals(@HeaderParam("Authorization") String bearer,
      @QueryParam("ippis") String ippis, @QueryParam("id") Long lastID,
      @QueryParam("year") String year) {
    return Response.ok(service.getWithdrawals(userManager.getUser(bearer),
        ippis, lastID, year)).build();
  }

  @Secure
  @Path("user/withdrawals/create")
  @POST
  public Response createWithdrawal(@HeaderParam("Authorization") String bearer,
      AmountBean amountBean) {
    return Response.ok(service.createWithdrawal(userManager.getUser(bearer),
        amountBean.getName(), amountBean.getAmount())).build();
  }

  @Secure
  @Path("withdrawals/approve")
  @POST
  public Response approveWithdrawal(@HeaderParam("Authorization") String bearer,
      ApproveWithdrawal approveWithdrawal) {
    return Response.ok(service.approveWithdrawal(userManager.getUser(bearer),
        approveWithdrawal.getId(), approveWithdrawal.getStatus()))
        .build();
  }

  @Secure
  @Path("user/withdrawals/awaiting")
  @GET
  public Response getAwaitingWithdrawal(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long id) {
    return Response.ok(service.getAwaitingWithdrawal(userManager.getUser(bearer),
        id)).build();
  }

  @Secure
  @Path("users")
  @GET
  public Response getUsers(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID) {
    return Response.ok(service.getUsers(userManager.getUser(bearer), lastID))
        .build();
  }

  @Secure
  @Path("users/search")
  @GET
  public Response getUsers(@HeaderParam("Authorization") String bearer,
      @QueryParam("ippis") String ippis) {
    return Response.ok(service.searchUsers(userManager.getUser(bearer), ippis))
        .build();
  }

  @Secure
  @Path("user/delete")
  @GET
  public Response deleteUser(@HeaderParam("Authorization") String bearer,
      @QueryParam("ippis") String ippis) {
    return Response.ok(service.deleteUser(userManager.getUser(bearer), ippis))
        .build();
  }

  @Secure
  @Path("user/info")
  @POST
  public Response updateInfo(@HeaderParam("Authorization") String bearer,
      UserBean userBean) {
    return Response.ok(service.updateInfo(userManager
        .getUser(bearer), userBean)).build();
  }

  @Secure
  @Path("activities/1")
  @GET
  public Response getAdmin1Activities(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID) {
    return Response.ok(service.getAdmin1Activities(userManager.getUser(bearer), lastID))
        .build();
  }

  @Secure
  @Path("activities/all")
  @GET
  public Response getAllActivities(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID) {
    return Response.ok(service.getAllActivities(userManager.getUser(bearer), lastID))
        .build();
  }

  @Secure
  @Path("user/loans/create")
  @POST
  public Response createLoan(@HeaderParam("Authorization") String bearer,
      LoanBean loanBean) {
    User user = userManager.getUser(bearer);
    return Response.ok(service.createLoan(user, loanBean))
        .build();
  }

  @Secure
  @Path("user/loans/approve")
  @POST
  public Response approveLoan(@HeaderParam("Authorization") String bearer,
      ApproveLoan approveLoan) {
    User user = userManager.getUser(bearer);
    return Response.ok(service.approveLoan(user, approveLoan.getId(),
        approveLoan.getApproval()))
        .build();
  }

  @Secure
  @Path("user/loans/approve")
  @GET
  public Response getUserApprovedLoans(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long id) {
    User user = userManager.getUser(bearer);
    return Response.ok(service.getUserApprovedLoans(user, id)).build();
  }

  @Secure
  @Path("user/loans/awaiting")
  @GET
  public Response getAwaitingLoan(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID) {
    return Response.ok(service.getAwaitingLoans(userManager.getUser(bearer), lastID))
        .build();
  }

  @Secure
  @Path("user/loans/type")
  @GET
  public Response getLoansByType(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID, @QueryParam("type") Loan.Type type) {
    return Response.ok(service.getUserApprovedLoansByType(userManager
        .getUser(bearer), lastID, type)).build();
  }

  @Secure
  @Path("user/loans")
  @GET
  public Response getUserLoans(@HeaderParam("Authorization") String bearer,
      @QueryParam("ippis") String ippis) {
    return Response.ok(service.getAllUserActiveLoans(ippis)).build();
  }

  @Secure
  @Path("user/loans/all")
  @GET
  public Response getAllLoan(@HeaderParam("Authorization") String bearer,
      @QueryParam("id") Long lastID) {
    return Response.ok(service.getAllActiveLoans(userManager.getUser(bearer), lastID))
        .build();
  }

  @Secure
  @Path("loan/repayment")
  @POST
  public Response updateRepayment(RepaymentBean repaymentBean) {
    return Response.ok(service.updateLoanRepayment(repaymentBean)).build();
  }

  @Secure
  @Path("loan/repayments")
  @GET
  public Response updateRepayment(@QueryParam("id") Long loanId) {
    return Response.ok(service.getRepayments(loanId)).build();
  }

  @Secure
  @Path("user/summary")
  @GET
  public Response getSummary(@HeaderParam("Authorization") String bearer,
      @QueryParam("year") Integer year) {
    return Response.ok(service.getSummary(userManager.getUser(bearer), year))
        .build();
  }

  @Secure
  @Path("users/download")
  @GET
  @Produces("application/vnd.ms-excel")
  public Response downloadUsers(@HeaderParam("Authorization") String bearer,
      @QueryParam("month") String month)
      throws IOException {
    User user = userManager.getUser(bearer);
    BaseResponse baseResponse = service.downloadUsers(user, month);
    File file = new File((String) baseResponse.getData());
    ResponseBuilder response = Response.ok((Object) file);
    response.header("Content-Disposition", "attachment; filename=" + "\"" + file.getPath() + "\"");
    return response.build();

  }
}
