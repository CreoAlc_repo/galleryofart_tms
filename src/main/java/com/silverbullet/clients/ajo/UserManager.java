/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo;

import com.silverbullet.clients.ajo.entities.User;
import com.silverbullet.gaora.ejb.CacheManager;
import com.silverbullet.gaora.entities.Auth;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class UserManager {

    @EJB
    private CacheManager<String, Auth> cacheManager;

    public User getUser(String bearer) {
        if (Objects.isNull(bearer) || bearer.isEmpty()) {
            return null;
        } else {
            Auth auth = getUserAuth(bearer);
            if (Objects.isNull(auth)) {
                return null;
            } else {
                return (User) auth.getV();
            }
        }
    }

    public Auth getUserAuth(String bearer) {
        String token = getToken(bearer);
        if (Objects.isNull(token)) {
            return null;
        } else {
            Auth auth = cacheManager.get(token);
            return auth;
        }
    }

    public String getToken(String bearer) {
        if (Objects.isNull(bearer) || bearer.isEmpty()) {
            return null;
        } else {
            return bearer.split("\\s+")[1];
        }
    }
}
