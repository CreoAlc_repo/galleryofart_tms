/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author prodigy4440
 */
@NamedQueries({
    @NamedQuery(name = "Loan.findByIppis",
            query = "SELECT l From Loan l WHERE l.user.ippisNumber = :ippis AND l.id <= :id ORDER BY l.id DESC")
    ,
    @NamedQuery(name = "Loan.findByApproval",
            query = "SELECT l FROM Loan l WHERE l.approval = :approval AND l.id "
            + "<= :id ORDER BY l.id DESC")
    ,
    @NamedQuery(name = "Loan.findByStatus",
            query = "SELECT l FROM Loan l WHERE l.status = :status AND l.id "
            + "<= :id ORDER BY l.id DESC")
    ,
    @NamedQuery(name = "Loan.findByNotStatus",
            query = "SELECT l FROM Loan l WHERE l.status != :status AND l.user.ippisNumber "
            + "= :ippis ORDER BY l.id DESC")
    ,
    @NamedQuery(name = "Loan.findByStatuses",
            query = "SELECT l FROM Loan l WHERE l.status IN (:status) AND l.id <= :id "
            + "ORDER BY l.createdDate DESC")
    ,
    @NamedQuery(name = "Loan.findByUserAndStatuses",
            query = "SELECT l FROM Loan l WHERE l.user.ippisNumber = :ippis AND "
            + "l.status IN (:status) AND l.id <= :id ORDER BY l.id DESC")
    ,
    @NamedQuery(name = "Loan.findByIppisAndMonth",
            query = "SELECT l FROM Loan l WHERE l.user.ippisNumber = :ippis AND "
            + "(YEAR(l.createdDate) = YEAR(NOW()) AND MONTHNAME(l.createdDate) = :month)")
    ,
    @NamedQuery(name = "Loan.findForRepaymentUsingIppis", 
            query = "SELECT l FROM Loan l WHERE l.user.ippisNumber = :ippis AND "
                    + "l.approval = :approval AND l.status <> :status ORDER BY l.createdDate DESC")
})
@Entity
@Table(name = "loan")
public class Loan implements Serializable {

    private static final long serialVersionUID = 6324619328812309936L;

    @Column(name = "id", nullable = false, unique = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double amount;

    private Double paid;

    private Double repayment;

    @Column(name = "number_of_months")
    private Integer numberOfMonths;

    @Column(name = "skip_months")
    private Integer skipMonths;

    @Column(name = "skipped")
    private Integer skipped;

    @Enumerated(EnumType.STRING)
    private Approval approval;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @JsonBackReference
    @OneToMany(mappedBy = "loan", cascade = CascadeType.ALL, fetch = FetchType.EAGER,
            orphanRemoval = true)
    @OrderBy("date ASC")
    private Set<LoanPaymentHistory> loanPayments;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "user", referencedColumnName = "id")
    private User user;

    public Loan() {
    }

    public Loan(Double amount, Double paid, Double repayment, Integer numberOfMonths,
            Integer skipMonths, Integer skipped, Approval released, Status status,
            Type type, Date createdDate, User user) {
        this.amount = amount;
        this.paid = paid;
        this.repayment = repayment;
        this.numberOfMonths = numberOfMonths;
        this.skipMonths = skipMonths;
        this.skipped = skipped;
        this.approval = released;
        this.status = status;
        this.type = type;
        this.createdDate = createdDate;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPaid() {
        return paid;
    }

    public void setPaid(Double paid) {
        this.paid = paid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.id);
        hash = 13 * hash + Objects.hashCode(this.amount);
        hash = 13 * hash + Objects.hashCode(this.paid);
        hash = 13 * hash + Objects.hashCode(this.status);
        hash = 13 * hash + Objects.hashCode(this.type);
        hash = 13 * hash + Objects.hashCode(this.createdDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Loan other = (Loan) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.paid, other.paid)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        return true;
    }

    public Double getRepayment() {
        return repayment;
    }

    public void setRepayment(Double repayment) {
        this.repayment = repayment;
    }

    public Integer getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(Integer numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public Integer getSkipMonths() {
        return skipMonths;
    }

    public void setSkipMonths(Integer skipMonths) {
        this.skipMonths = skipMonths;
    }

    public Integer getSkipped() {
        return skipped;
    }

    public void setSkipped(Integer skipped) {
        this.skipped = skipped;
    }

    public Approval getApproval() {
        return approval;
    }

    public void setApproval(Approval released) {
        this.approval = released;
    }

    public Set<LoanPaymentHistory> getLoanPayments() {
        return loanPayments;
    }

    public void setLoanPayments(Set<LoanPaymentHistory> loanPayments) {
        this.loanPayments = loanPayments;
    }

    public enum Approval {
        PENDING, APPROVED, DECLINED
    }

    public enum Status {
        CLEARED, PARTIAL, UNCLEAR, MERGED;
    }

    public enum Type {
        REGULAR, COMMODITY, SPECIAL
    }

}
