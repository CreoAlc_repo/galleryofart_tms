/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author prodigy4440
 */
@NamedQueries({
    @NamedQuery(name = "Saving.findByIppis",
            query = "SELECT s FROM Saving s WHERE s.user.ippisNumber = :ippis "
            + "AND s.id <= :id ORDER BY s.createdDate DESC")
    ,
    @NamedQuery(name = "Saving.findLastByIppis",
            query = "SELECT s FROM Saving s WHERE s.user.ippisNumber = :ippis ORDER BY s.id DESC")
    ,
    @NamedQuery(name = "Saving.findByIppisAndYear",
            query = "SELECT s FROM Saving s WHERE s.user.ippisNumber = :ippis AND "
            + "YEAR(s.createdDate) = :year AND s.id <= :id ORDER BY s.createdDate DESC"),
    @NamedQuery(name = "Saving.findByIppisAndMonth",
            query = "SELECT s FROM Saving s WHERE s.user.ippisNumber = :ippis AND "
                    + "(YEAR(s.createdDate) = YEAR(NOW()) AND MONTHNAME(s.createdDate) = :month)")
})
@Entity
@Table(name = "saving")
public class Saving implements Serializable {

    private static final long serialVersionUID = 4555270270594502066L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double amount;

    private String remarks;

    private Double balance;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "user",referencedColumnName = "id")
    private User user;

    public Saving() {
    }

    public Saving(Double amount, String remarks, Type type, Date createdDate,
            User user) {
        this.amount = amount;
        this.remarks = remarks;
        this.type = type;
        this.createdDate = createdDate;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @PreRemove
    private void preRemove() {
        if (user != null) {
            user = null;
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.amount);
        hash = 37 * hash + Objects.hashCode(this.remarks);
        hash = 37 * hash + Objects.hashCode(this.type);
        hash = 37 * hash + Objects.hashCode(this.createdDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Saving other = (Saving) obj;
        if (!Objects.equals(this.remarks, other.remarks)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Saving{" + "id=" + id + ", amount=" + amount + ", remarks=" + remarks + ", balance=" + balance + ", type=" + type + ", createdDate=" + createdDate + '}';
    }
    
    public enum Type {
        NORMAL, SPECIAL;
    }

}
