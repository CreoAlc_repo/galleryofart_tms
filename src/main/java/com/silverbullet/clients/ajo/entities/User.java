/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author prodigy4440
 */
@NamedQueries({
    @NamedQuery(name = "User.findByIppisNumber",
            query = "SELECT u FROM User u WHERE u.ippisNumber = :number")
    ,
    @NamedQuery(name = "User.searchForUser",
            query = "SELECT u FROM User u WHERE u.ippisNumber = :number "
            + "OR u.email = :number OR u.phoneNumber = :number")
    ,
    @NamedQuery(name = "User.findAll",
            query = "SELECT u FROM User u WHERE u.id <= :id ORDER BY u.id DESC")
})
@Entity
@Table(name = "ippis_user")
public class User implements Serializable {

    private static final long serialVersionUID = -4615846884439880197L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ippis_number")
    private String ippisNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "contribution_amount")
    private Double contributionAmount;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "balance_before")
    private Double balanceBefore;

    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Saving> savings;

    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Withdrawal> withdrawals;

    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Balance> balances;

    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Loan> loans;

    public User() {
    }

    public User(String ippisNumber, String name, String phoneNumber, String email,
            Role role, Double contributionAmount, String password, Date createdDate,
            Set<Saving> savings, Set<Withdrawal> withdrawals, Set<Balance> balances,
            Set<Loan> loans) {
        this.ippisNumber = ippisNumber;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.role = role;
        this.contributionAmount = contributionAmount;
        this.password = password;
        this.createdDate = createdDate;
        this.savings = savings;
        this.withdrawals = withdrawals;
        this.balances = balances;
        this.loans = loans;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIppisNumber() {
        return ippisNumber;
    }

    public void setIppisNumber(String ippisNumber) {
        this.ippisNumber = ippisNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getContributionAmount() {
        return contributionAmount;
    }

    public void setContributionAmount(Double contributionAmount) {
        this.contributionAmount = contributionAmount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Set<Saving> getSavings() {
        return savings;
    }

    public void setSavings(Set<Saving> savings) {
        this.savings = savings;
    }

    public Set<Withdrawal> getWithdrawals() {
        return withdrawals;
    }

    public void setWithdrawals(Set<Withdrawal> withdrawals) {
        this.withdrawals = withdrawals;
    }

    public Set<Balance> getBalances() {
        return balances;
    }

    public void setBalances(Set<Balance> balances) {
        this.balances = balances;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.ippisNumber);
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + Objects.hashCode(this.phoneNumber);
        hash = 17 * hash + Objects.hashCode(this.email);
        hash = 17 * hash + Objects.hashCode(this.contributionAmount);
        hash = 17 * hash + Objects.hashCode(this.password);
        hash = 17 * hash + Objects.hashCode(this.createdDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.ippisNumber, other.ippisNumber)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.phoneNumber, other.phoneNumber)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.contributionAmount, other.contributionAmount)) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", ippisNumber=" + ippisNumber
                + ", name=" + name + ", phoneNumber=" + phoneNumber
                + ", email=" + email + ", contributionAmount=" + contributionAmount
                + ", password=" + password + ", createdDate=" + createdDate + '}';
    }

    public Set<Loan> getLoans() {
        return loans;
    }

    public void setLoans(Set<Loan> loans) {
        if (this.loans == null) {

            this.loans = loans;
        } else {
            this.loans.clear();
            this.loans.addAll(loans);
        }
    }

    public Double getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(Double balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public enum Role {
        NORMAL, ADMIN_1, ADMIN_2, ADMIN_3;
    }

    @PreRemove
    private void preRemove() {
        if (!savings.isEmpty()) {
            for (Saving saving : savings) {
                saving.setUser(null);;
            }
        }
        if (!withdrawals.isEmpty()) {
            for (Withdrawal withdrawal : withdrawals) {
                withdrawal.setUser(null);
            }
        }

        if (!balances.isEmpty()) {
            for (Balance balance : balances) {
                balance.setUser(null);
            }
        }
    }
    
}
