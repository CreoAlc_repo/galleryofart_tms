/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PreRemove;
import javax.persistence.Table;

/**
 *
 * @author prodigy4440
 */
@NamedQueries({
  @NamedQuery(name = "Balance.findAll",
      query = "SELECT b FROM Balance b WHERE b.id <= :id ORDER BY b.id DESC")
  ,
    @NamedQuery(name = "Balance.currentYear",
      query = "SELECT b FROM Balance b WHERE b.user.ippisNumber = :ippis "
      + "AND b.year = YEAR(NOW())")
  ,
    @NamedQuery(name = "Balance.findByYear",
      query = "SELECT b FROM Balance b WHERE b.user.ippisNumber = :ippis "
      + "AND b.year = :year")
})
@Entity
@Table(name = "balance")
public class Balance implements Serializable {

  @Id
  @Column(name = "id", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private static final long serialVersionUID = 1382948450198312287L;

  @Column(name = "balance_before")
  private Double balanceBefore;

  @Column(name = "current_balance")
  private Double currentBalance;

  @Column(name = "closing_balance")
  private Double closingBalance;

  @Column(name = "loan_before")
  private Double loanBefore;

  @Column(name = "total_loan")
  private Double totalLoan;

  @Column(name = "paid_loan")
  private Double paidLoan;

  @Column(name = "unpaid_loan")
  private Double unclearedLoan;

  @Column(name = "current_year")
  private String year;

  @JsonManagedReference
  @ManyToOne
  @JoinColumn(name = "user", referencedColumnName = "id")
  private User user;

  public Balance() {
  }

  public Balance(Double balanceBefore, Double currentBalance, Double closingBalance,
      Double totalLoan, Double paidLoan, Double unclearedLoan, String year,
      User user) {
    this.balanceBefore = balanceBefore;
    this.currentBalance = currentBalance;
    this.closingBalance = closingBalance;
    this.totalLoan = totalLoan;
    this.paidLoan = paidLoan;
    this.unclearedLoan = unclearedLoan;
    this.year = year;
    this.user = user;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getBalanceBefore() {
    return balanceBefore;
  }

  public void setBalanceBefore(Double balanceBefore) {
    this.balanceBefore = balanceBefore;
  }

  public Double getCurrentBalance() {
    return currentBalance;
  }

  public void setCurrentBalance(Double currentBalance) {
    this.currentBalance = currentBalance;
  }

  public Double getClosingBalance() {
    return closingBalance;
  }

  public void setClosingBalance(Double closingBalance) {
    this.closingBalance = closingBalance;
  }

  public Double getUnclearedLoan() {
    return unclearedLoan;
  }

  public void setUnclearedLoan(Double unclearedLoan) {
    this.unclearedLoan = unclearedLoan;
  }

  public Double getTotalLoan() {
    return totalLoan;
  }

  public void setTotalLoan(Double totalLoan) {
    this.totalLoan = totalLoan;
  }

  public Double getPaidLoan() {
    return paidLoan;
  }

  public void setPaidLoan(Double paidLoan) {
    this.paidLoan = paidLoan;
  }

  public Double getLoanBefore() {
    return loanBefore;
  }

  public void setLoanBefore(Double loanBefore) {
    this.loanBefore = loanBefore;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 47 * hash + Objects.hashCode(this.id);
    hash = 47 * hash + Objects.hashCode(this.balanceBefore);
    hash = 47 * hash + Objects.hashCode(this.currentBalance);
    hash = 47 * hash + Objects.hashCode(this.closingBalance);
    hash = 47 * hash + Objects.hashCode(this.year);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Balance other = (Balance) obj;
    if (!Objects.equals(this.year, other.year)) {
      return false;
    }
    if (!Objects.equals(this.id, other.id)) {
      return false;
    }
    if (!Objects.equals(this.balanceBefore, other.balanceBefore)) {
      return false;
    }
    if (!Objects.equals(this.currentBalance, other.currentBalance)) {
      return false;
    }
    if (!Objects.equals(this.closingBalance, other.closingBalance)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Balance{" + "id=" + id + ", balanceBefore=" + balanceBefore
        + ", currentBalance=" + currentBalance + ", closingBalance="
        + closingBalance + ", year=" + year + '}';
  }

  @PreRemove
  private void preRemove() {
    if (user != null) {
      user = null;
    }
  }
}
