/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author prodigy4440
 */
@NamedQueries({
    @NamedQuery(name = "Withdrawal.findByIppis",
            query = "SELECT w FROM Withdrawal w WHERE w.user.ippisNumber = :ippis "
            + "AND w.id <= :id AND w.status = :status ORDER BY w.id DESC")
    ,
    @NamedQuery(name = "Withdrawal.findByIppisAndYear",
            query = "SELECT w FROM Withdrawal w WHERE w.user.ippisNumber = :ippis "
                    + "AND YEAR(w.createdDate) = :year AND w.id <= :id AND w.status "
                    + "= :status ORDER BY w.createdDate DESC")
    ,
    @NamedQuery(name = "Withdrawal.findAllByStatus",
            query = "SELECT w FROM Withdrawal w WHERE w.status = :status "
            + "AND w.id <= :id ORDER BY w.id DESC")
})
@Entity
@Table(name = "withdrawal")
public class Withdrawal implements Serializable {

    private static final long serialVersionUID = 5721178638384463528L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double amount;

    private Double balance;

    private String remarks;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "user",referencedColumnName = "id")
    private User user;

    public Withdrawal() {
    }

    public Withdrawal(Double amount, String remarks, Date createdDate,
            User user) {
        this.amount = amount;
        this.remarks = remarks;
        this.createdDate = createdDate;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.amount);
        hash = 47 * hash + Objects.hashCode(this.remarks);
        hash = 47 * hash + Objects.hashCode(this.createdDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Withdrawal other = (Withdrawal) obj;
        if (!Objects.equals(this.remarks, other.remarks)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        return Objects.equals(this.createdDate, other.createdDate);
    }

    @PreRemove
    private void preRemove() {
        if (user != null) {
            user = null;
        }
    }

    @Override
    public String toString() {
        return "Withdrawal{" + "id=" + id + ", amount=" + amount + ", balance=" + balance + ", remarks=" + remarks + ", status=" + status + ", createdDate=" + createdDate + '}';
    }

    public enum Status {
        APPROVED, DECLINED, PENDING;
    }
}
