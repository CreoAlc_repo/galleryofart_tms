/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo;

import com.silverbullet.clients.ajo.util.DataProcessor;
import com.silverbullet.gaora.ejb.ConcurrencyManager;
import com.silverbullet.gaora.ejb.PasswordManager;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author prodigy4440
 */
@Singleton
@Startup
public class StartService {

    @EJB
    private PasswordManager passwordManager;

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private ConcurrencyManager concurrencyManager;

    @EJB
    private SmsManager smsManager;

    @EJB
    private RunnerService runnerService;

    @PostConstruct
    public void init() {
//        smsManager.sendSms("Deploying contribution Web App", "08074209323");
//        try {
//            DataProcessor.loadS2016("/Users/macbookpro/Documents/nga/2016.csv",
//                    entityManager, passwordManager);
//            DataProcessor.loadS2017("/Users/macbookpro/Documents/nga/2017savings.csv",
//                    entityManager, passwordManager);
//            DataProcessor.loadL2017("/Users/macbookpro/Documents/nga/2017loans.csv",
//                    entityManager, passwordManager);
//        } catch (IOException ex) {
//            Logger.getLogger(StartService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        List<User> users = entityManager.createQuery("SELECT u FROM User u WHERE u.ippisNumber = :ippis", User.class)
//                .setParameter("ippis", "").getResultList();
//        if (!users.isEmpty()) {
//            for (User user : users) {
//                entityManager.remove(user);
//            }
//        }
//        runnerService.monthlySavingSchedule();
//        runnerService.monthlyLoanSchedule();
    }

}
