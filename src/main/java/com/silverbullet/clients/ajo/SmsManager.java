/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo;

import com.silverbullet.clients.ajo.util.SmsUtil;
import com.silverbullet.gaora.util.MessageLogger;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class SmsManager {

    private SmsUtil smsUtil;

    @PostConstruct
    public void init() {
        smsUtil = new SmsUtil();
    }

    @Asynchronous
    public void sendSms(String message, String sentTo) {
        try {
            smsUtil.sendSms(message, "NGA cooperative", sentTo);
        } catch (IOException ex) {
            MessageLogger.info(SmsManager.class, ex.getMessage());
        }
    }
}
