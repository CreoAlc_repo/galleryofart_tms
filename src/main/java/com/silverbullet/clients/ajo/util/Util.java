/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

import com.silverbullet.gaora.util.DateTimeUtil;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.util.Date;

/**
 *
 * @author prodigy4440
 */
public class Util {

  public static Date getCurrentMonthEnd() {
    return getMonthEnd(LocalDate.now().getMonth(), Year.now());
  }

  public static Date getMonthEnd(Month month, Year year) {
    return DateTimeUtil.asDate(YearMonth.of(year.getValue(), month).atEndOfMonth());
  }

  public static String getCurrentYear() {
    return Year.now().toString();
  }

  public static String getMonthFromDate(Date date) {
    return Month.of(date.getMonth()).toString();
  }

  public static Date toPayDay(int year, Month month) {
    return DateTimeUtil.asDate(LocalDate.of(year, month, 7));
  }

  public static void main(String[] args) {
    for (int i = 1; i < 10; i++) {
      for (int j = 0; j < 5; j++) {
        if (i == j) {
          continue;
        }
        System.out.println(i + " - " + j);
      }
    }
  }
}
