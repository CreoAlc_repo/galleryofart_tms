/*
 * Copyright 2017 prodigy4440.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.silverbullet.clients.ajo.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author prodigy4440
 */
public class ObjectUtils {

    private ObjectUtils() {
    }

    public static Map<String, Object> getFieldNamesAndValues(final Object obj,
            boolean publicOnly) throws IllegalArgumentException, IllegalAccessException {
        Class<? extends Object> c1 = obj.getClass();
        Map<String, Object> map = new HashMap<>();
        Field[] fields = c1.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            String name = fields[i].getName();
            if (publicOnly) {
                if (Modifier.isPublic(fields[i].getModifiers())) {
                    Object value = fields[i].get(obj);
                    map.put(name, value);
                }
            } else {
                fields[i].setAccessible(true);
                Object value = fields[i].get(obj);
                map.put(name, value);
            }
        }
        return map;
    }

    public static String getFieldNames(Object obj) {
        Class<? extends Object> clazz = obj.getClass();
        StringBuilder builder = new StringBuilder();
        Map<String, Object> map = new HashMap<>();
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            String name = fields[i].getName();
            fields[i].setAccessible(true);

            if (builder.length() != 0) {
                builder.append(",");
            }
            builder.append(name);
        }
        return builder.toString();
    }

    public static String getFieldValues(Object obj) {
        Class<? extends Object> c1 = obj.getClass();
        Field[] fields = c1.getDeclaredFields();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            if (builder.length() != 0) {
                builder.append(",");
            }
            Object value = null;
            try {
                value = fields[i].get(obj);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
            builder.append(value);
        }
        return builder.toString();
    }

}
