/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

/**
 *
 * @author prodigy4440
 */
public class FileRow {

    private String sn;
    private String ippis;
    private String name;
    private String balanceBefore;
    private String januarySavings;
    private String januaryWithdrawal;
    private String februarySavings;
    private String februaryWithdrawal;
    private String marchSavings;
    private String marchWithdrawal;
    private String aprilSavings;
    private String aprilWithdrawal;
    private String maySavings;
    private String mayWithdrawal;
    private String juneSavings;
    private String juneWithdrawal;
    private String julySavings;
    private String julyWithdrawal;
    private String augustSavings;
    private String augustWithdrawal;
    private String septemberSavings;
    private String septemberWithdrawal;
    private String octoberSavings;
    private String octoberWithdrawal;
    private String novemberSavings;
    private String novemberWithdrawal;
    private String decemberSavings;
    private String decemberWithdrawal;
    private String totalSavingsSavings;
    private String totalWithdrawal;
    private String closingBalance;
    private String phoneNumber;
    private String email;

    public FileRow() {
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getIppis() {
        return ippis;
    }

    public void setIppis(String ippis) {
        this.ippis = ippis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(String balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public String getJanuarySavings() {
        return januarySavings;
    }

    public void setJanuarySavings(String januarySavings) {
        this.januarySavings = januarySavings;
    }

    public String getJanuaryWithdrawal() {
        return januaryWithdrawal;
    }

    public void setJanuaryWithdrawal(String januaryWithdrawal) {
        this.januaryWithdrawal = januaryWithdrawal;
    }

    public String getFebruarySavings() {
        return februarySavings;
    }

    public void setFebruarySavings(String februarySavings) {
        this.februarySavings = februarySavings;
    }

    public String getFebruaryWithdrawal() {
        return februaryWithdrawal;
    }

    public void setFebruaryWithdrawal(String februaryWithdrawal) {
        this.februaryWithdrawal = februaryWithdrawal;
    }

    public String getMarchSavings() {
        return marchSavings;
    }

    public void setMarchSavings(String marchSavings) {
        this.marchSavings = marchSavings;
    }

    public String getMarchWithdrawal() {
        return marchWithdrawal;
    }

    public void setMarchWithdrawal(String marchWithdrawal) {
        this.marchWithdrawal = marchWithdrawal;
    }

    public String getAprilSavings() {
        return aprilSavings;
    }

    public void setAprilSavings(String aprilSavings) {
        this.aprilSavings = aprilSavings;
    }

    public String getAprilWithdrawal() {
        return aprilWithdrawal;
    }

    public void setAprilWithdrawal(String aprilWithdrawal) {
        this.aprilWithdrawal = aprilWithdrawal;
    }

    public String getMaySavings() {
        return maySavings;
    }

    public void setMaySavings(String maySavings) {
        this.maySavings = maySavings;
    }

    public String getMayWithdrawal() {
        return mayWithdrawal;
    }

    public void setMayWithdrawal(String mayWithdrawal) {
        this.mayWithdrawal = mayWithdrawal;
    }

    public String getJuneSavings() {
        return juneSavings;
    }

    public void setJuneSavings(String juneSavings) {
        this.juneSavings = juneSavings;
    }

    public String getJuneWithdrawal() {
        return juneWithdrawal;
    }

    public void setJuneWithdrawal(String juneWithdrawal) {
        this.juneWithdrawal = juneWithdrawal;
    }

    public String getJulySavings() {
        return julySavings;
    }

    public void setJulySavings(String julySavings) {
        this.julySavings = julySavings;
    }

    public String getJulyWithdrawal() {
        return julyWithdrawal;
    }

    public void setJulyWithdrawal(String julyWithdrawal) {
        this.julyWithdrawal = julyWithdrawal;
    }

    public String getAugustSavings() {
        return augustSavings;
    }

    public void setAugustSavings(String augustSavings) {
        this.augustSavings = augustSavings;
    }

    public String getAugustWithdrawal() {
        return augustWithdrawal;
    }

    public void setAugustWithdrawal(String augustWithdrawal) {
        this.augustWithdrawal = augustWithdrawal;
    }

    public String getSeptemberSavings() {
        return septemberSavings;
    }

    public void setSeptemberSavings(String septemberSavings) {
        this.septemberSavings = septemberSavings;
    }

    public String getSeptemberWithdrawal() {
        return septemberWithdrawal;
    }

    public void setSeptemberWithdrawal(String septemberWithdrawal) {
        this.septemberWithdrawal = septemberWithdrawal;
    }

    public String getOctoberSavings() {
        return octoberSavings;
    }

    public void setOctoberSavings(String octoberSavings) {
        this.octoberSavings = octoberSavings;
    }

    public String getOctoberWithdrawal() {
        return octoberWithdrawal;
    }

    public void setOctoberWithdrawal(String octoberWithdrawal) {
        this.octoberWithdrawal = octoberWithdrawal;
    }

    public String getNovemberSavings() {
        return novemberSavings;
    }

    public void setNovemberSavings(String novemberSavings) {
        this.novemberSavings = novemberSavings;
    }

    public String getNovemberWithdrawal() {
        return novemberWithdrawal;
    }

    public void setNovemberWithdrawal(String novemberWithdrawal) {
        this.novemberWithdrawal = novemberWithdrawal;
    }

    public String getDecemberSavings() {
        return decemberSavings;
    }

    public void setDecemberSavings(String decemberSavings) {
        this.decemberSavings = decemberSavings;
    }

    public String getDecemberWithdrawal() {
        return decemberWithdrawal;
    }

    public void setDecemberWithdrawal(String decemberWithdrawal) {
        this.decemberWithdrawal = decemberWithdrawal;
    }

    public String getTotalSavingsSavings() {
        return totalSavingsSavings;
    }

    public void setTotalSavingsSavings(String totalSavingsSavings) {
        this.totalSavingsSavings = totalSavingsSavings;
    }

    public String getTotalWithdrawal() {
        return totalWithdrawal;
    }

    public void setTotalWithdrawal(String totalWithdrawal) {
        this.totalWithdrawal = totalWithdrawal;
    }

    public String getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(String closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Row{" + "sn=" + sn + ", ippis=" + ippis + ", name=" + name + ", balanceBefore=" + balanceBefore + ", januarySavings=" + januarySavings + ", januaryWithdrawal=" + januaryWithdrawal + ", februarySavings=" + februarySavings + ", februaryWithdrawal=" + februaryWithdrawal + ", marchSavings=" + marchSavings + ", marchWithdrawal=" + marchWithdrawal + ", aprilSavings=" + aprilSavings + ", aprilWithdrawal=" + aprilWithdrawal + ", maySavings=" + maySavings + ", mayWithdrawal=" + mayWithdrawal + ", juneSavings=" + juneSavings + ", juneWithdrawal=" + juneWithdrawal + ", julySavings=" + julySavings + ", julyWithdrawal=" + julyWithdrawal + ", augustSavings=" + augustSavings + ", augustWithdrawal=" + augustWithdrawal + ", septemberSavings=" + septemberSavings + ", septemberWithdrawal=" + septemberWithdrawal + ", octoberSavings=" + octoberSavings + ", octoberWithdrawal=" + octoberWithdrawal + ", novemberSavings=" + novemberSavings + ", novemberWithdrawal=" + novemberWithdrawal + ", decemberSavings=" + decemberSavings + ", decemberWithdrawal=" + decemberWithdrawal + ", totalSavingsSavings=" + totalSavingsSavings + ", totalWithdrawal=" + totalWithdrawal + ", closingBalance=" + closingBalance + ", phoneNumber=" + phoneNumber + ", email=" + email + '}';
    }

}
