/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 */
public class ExportData implements Serializable{
    
    private String ippis;
    private String name;
    private double monthlySaving;
    private double saving;
    private double monthlyLoanDeduction;

    public ExportData() {
    }

    public String getIppis() {
        return ippis;
    }

    public void setIppis(String ippis) {
        this.ippis = ippis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMonthlySaving() {
        return monthlySaving;
    }

    public void setMonthlySaving(double monthlySaving) {
        this.monthlySaving = monthlySaving;
    }

    public double getSaving() {
        return saving;
    }

    public void setSaving(double saving) {
        this.saving = saving;
    }

    public double getMonthlyLoanDeduction() {
        return monthlyLoanDeduction;
    }

    public void setMonthlyLoanDeduction(double monthlyLoanDeduction) {
        this.monthlyLoanDeduction = monthlyLoanDeduction;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.ippis);
        hash = 23 * hash + Objects.hashCode(this.name);
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.monthlySaving) ^ (Double.doubleToLongBits(this.monthlySaving) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.saving) ^ (Double.doubleToLongBits(this.saving) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.monthlyLoanDeduction) ^ (Double.doubleToLongBits(this.monthlyLoanDeduction) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExportData other = (ExportData) obj;
        if (Double.doubleToLongBits(this.monthlySaving) != Double.doubleToLongBits(other.monthlySaving)) {
            return false;
        }
        if (Double.doubleToLongBits(this.saving) != Double.doubleToLongBits(other.saving)) {
            return false;
        }
        if (Double.doubleToLongBits(this.monthlyLoanDeduction) != Double.doubleToLongBits(other.monthlyLoanDeduction)) {
            return false;
        }
        if (!Objects.equals(this.ippis, other.ippis)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ExportData{" + "ippis=" + ippis + ", name=" + name + ", monthlySaving=" + monthlySaving + ", saving=" + saving + ", monthlyLoanDeduction=" + monthlyLoanDeduction + '}';
    }
    
}
