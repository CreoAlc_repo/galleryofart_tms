/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author prodigy4440
 */
public class Csv {

    private static final String DEST_FILE_PATH = "/home/prodigy4440/Desktop/contributors.csv";

    private static <T> String getHeader(T obj, int... exclude) {
        StringBuilder builder = new StringBuilder();
        Inflector inflector = Inflector.getInstance();
        List<Integer> excludes = new LinkedList<>();
        for (Integer in : exclude) {
            excludes.add(in);
        }

        String fieldNames = ObjectUtils.getFieldNames(obj);
        String[] fields = fieldNames.split(",");

        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            if (!excludes.contains(i)) {
                if (builder.length() != 0) {
                    builder.append(",");
                }
                builder.append(inflector.titleCase(inflector.splitCamelCase(field)));
            }
        }

        return builder.toString();
    }

    private static <T> String getValues(T object, int... exclude) {
        StringBuilder builder = new StringBuilder();
        List<Integer> excludes = new LinkedList<>();
        for (Integer in : exclude) {
            excludes.add(in);
        }

        String[] values = ObjectUtils.getFieldValues(object).split(",");
        for (int i = 0; i < values.length; i++) {

            if (!excludes.contains(i)) {
                String value = values[i];
                if (builder.length() != 0) {
                    builder.append(",");
                }
                builder.append(value);

            }

        }

        return builder.toString();
    }

    public static <T> String toFile(List<T> data, int... exclude) throws IOException {
        List<String> lines = new ArrayList<>();
        if (!data.isEmpty()) {
            T firstData = data.get(0);
            lines.add(getHeader(firstData, exclude));
            for (T t : data) {
                lines.add(getValues(t, exclude));
            }
        }
        Path filePath = Files.write(Paths.get(DEST_FILE_PATH), lines);
        return filePath.normalize().toString();
    }
//
//    public static void main(String[] args) throws IOException {
//        String text = "This is a sample line";
//        List<String> lines = Arrays.asList("Line 1", "Line 2", "Line 3", "Line 4");
//        Path paths = Files.write(Paths.get("/home/prodigy4440/Desktop/sample.txt"), lines);
//        System.out.println(paths.normalize().toString());
//    }

}
