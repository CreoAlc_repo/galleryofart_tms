/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

import com.silverbullet.clients.ajo.entities.Balance;
import com.silverbullet.clients.ajo.entities.Loan;
import com.silverbullet.clients.ajo.entities.LoanPaymentHistory;
import com.silverbullet.clients.ajo.entities.Saving;
import com.silverbullet.clients.ajo.entities.User;
import com.silverbullet.clients.ajo.entities.Withdrawal;
import com.silverbullet.gaora.ejb.PasswordManager;
import java.io.IOException;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.EntityManager;

/**
 *
 * @author prodigy4440
 */
public class DataProcessor {

  private static final String DELIMETER = ",(?=([^\"]|\"[^\"]*\")*$)";

  public static void loadS2016(String filePath, EntityManager entityManager, PasswordManager passwordManager) throws IOException {
    Iterator<String> mainData = new MMapFile(filePath).head();

    int count = 0;
    //Begin 2016 Data
    while (mainData.hasNext()) {
      String line = mainData.next();
      if (count == 0) {
        System.out.println(line);
      } else {
        if (!line.isEmpty()) {
          String stringYear = "2016";
          int intYear = 2016;

          double blc = toAmount(line, 3);
          double svn = 0.0;
          double wit = 0.0;

          double totalSavings = toAmount(line, 28);
          double totalwithdrawals = toAmount(line, 29);
          double closingBalance = toAmount(line, 30);

          Balance balance = new Balance();
          balance.setBalanceBefore(blc);
          balance.setClosingBalance(closingBalance);
          balance.setCurrentBalance(closingBalance);
          balance.setYear(stringYear);

          User user = new User();
          user.setIppisNumber(extract(line, 1));
          user.setName(extract(line, 2));
          user.setPhoneNumber(extract(line, 31));
          user.setBalanceBefore(blc);
          user.setEmail(extract(line, 32));
          user.setRole(User.Role.NORMAL);
          user.setContributionAmount(0.0);
          user.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));

          svn = toAmount(line, 4);
          blc = blc + svn;
          Saving janSaving = new Saving();
          janSaving.setAmount(svn);
          janSaving.setBalance(blc);
          janSaving.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));
          janSaving.setRemarks("System Generated");
          janSaving.setType(Saving.Type.NORMAL);
          janSaving.setUser(user);
          user.setContributionAmount(svn);

          wit = toAmount(line, 5);
          blc = blc - wit;
          Withdrawal janWithdrawal = new Withdrawal();
          janWithdrawal.setAmount(wit);
          janWithdrawal.setBalance(blc);
          janWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));
          janWithdrawal.setRemarks("System Generated");
          janWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          janWithdrawal.setUser(user);

          svn = toAmount(line, 6);
          blc = blc + svn;
          Saving febSaving = new Saving();
          febSaving.setAmount(svn);
          febSaving.setBalance(blc);
          febSaving.setCreatedDate(Util.toPayDay(intYear, Month.FEBRUARY));
          febSaving.setRemarks("System Generated");
          febSaving.setType(Saving.Type.NORMAL);
          febSaving.setUser(user);

          wit = toAmount(line, 7);
          blc = blc - wit;
          Withdrawal febWithdrawal = new Withdrawal();
          febWithdrawal.setAmount(wit);
          febWithdrawal.setBalance(blc);
          febWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.FEBRUARY));
          febWithdrawal.setRemarks("System Generated");
          febWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          febWithdrawal.setUser(user);

          svn = toAmount(line, 8);
          blc = blc + svn;
          Saving marchSaving = new Saving();
          marchSaving.setAmount(svn);
          marchSaving.setBalance(blc);
          marchSaving.setCreatedDate(Util.toPayDay(intYear, Month.MARCH));
          marchSaving.setRemarks("System Generated");
          marchSaving.setType(Saving.Type.NORMAL);
          marchSaving.setUser(user);

          wit = toAmount(line, 9);
          blc = blc - wit;
          Withdrawal marchWithdrawal = new Withdrawal();
          marchWithdrawal.setAmount(wit);
          marchWithdrawal.setBalance(blc);
          marchWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.MARCH));
          marchWithdrawal.setRemarks("System Generated");
          marchWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          marchWithdrawal.setUser(user);

          svn = toAmount(line, 10);
          blc = blc + svn;
          Saving aprilSaving = new Saving();
          aprilSaving.setAmount(svn);
          aprilSaving.setBalance(blc);
          aprilSaving.setCreatedDate(Util.toPayDay(intYear, Month.APRIL));
          aprilSaving.setRemarks("System Generated");
          aprilSaving.setType(Saving.Type.NORMAL);
          aprilSaving.setUser(user);

          wit = toAmount(line, 11);
          blc = blc - wit;
          Withdrawal aprilWithdrawal = new Withdrawal();
          aprilWithdrawal.setAmount(wit);
          aprilWithdrawal.setBalance(blc);
          aprilWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.APRIL));
          aprilWithdrawal.setRemarks("System Generated");
          aprilWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          aprilWithdrawal.setUser(user);

          svn = toAmount(line, 12);
          blc = blc + svn;
          Saving maySaving = new Saving();
          maySaving.setAmount(svn);
          maySaving.setBalance(blc);
          maySaving.setCreatedDate(Util.toPayDay(intYear, Month.MAY));
          maySaving.setRemarks("System Generated");
          maySaving.setType(Saving.Type.NORMAL);
          maySaving.setUser(user);

          wit = toAmount(line, 13);
          blc = blc - wit;
          Withdrawal mayWithdrawal = new Withdrawal();
          mayWithdrawal.setAmount(wit);
          mayWithdrawal.setBalance(blc);
          mayWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.MAY));
          mayWithdrawal.setRemarks("System Generated");
          mayWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          mayWithdrawal.setUser(user);

          svn = toAmount(line, 14);
          blc = blc + svn;
          Saving juneSaving = new Saving();
          juneSaving.setAmount(svn);
          juneSaving.setBalance(blc);
          juneSaving.setCreatedDate(Util.toPayDay(intYear, Month.JUNE));
          juneSaving.setRemarks("System Generated");
          juneSaving.setType(Saving.Type.NORMAL);
          juneSaving.setUser(user);

          wit = toAmount(line, 15);
          blc = blc - wit;
          Withdrawal juneWithdrawal = new Withdrawal();
          juneWithdrawal.setAmount(wit);
          juneWithdrawal.setBalance(blc);
          juneWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.JUNE));
          juneWithdrawal.setRemarks("System Generated");
          juneWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          juneWithdrawal.setUser(user);

          svn = toAmount(line, 16);
          blc = blc + svn;
          Saving julySaving = new Saving();
          julySaving.setAmount(svn);
          julySaving.setBalance(blc);
          julySaving.setCreatedDate(Util.toPayDay(intYear, Month.JULY));
          julySaving.setRemarks("System Generated");
          julySaving.setType(Saving.Type.NORMAL);
          julySaving.setUser(user);

          wit = toAmount(line, 17);
          blc = blc - wit;
          Withdrawal julyWithdrawal = new Withdrawal();
          julyWithdrawal.setAmount(wit);
          julyWithdrawal.setBalance(blc);
          julyWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.JULY));
          julyWithdrawal.setRemarks("System Generated");
          julyWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          julyWithdrawal.setUser(user);

          svn = toAmount(line, 18);
          blc = blc + svn;
          Saving augustSaving = new Saving();
          augustSaving.setAmount(svn);
          augustSaving.setBalance(blc);
          augustSaving.setCreatedDate(Util.toPayDay(intYear, Month.AUGUST));
          augustSaving.setRemarks("System Generated");
          augustSaving.setType(Saving.Type.NORMAL);
          augustSaving.setUser(user);

          wit = toAmount(line, 19);
          blc = blc - wit;
          Withdrawal augustWithdrawal = new Withdrawal();
          augustWithdrawal.setAmount(wit);
          augustWithdrawal.setBalance(blc);
          augustWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.AUGUST));
          augustWithdrawal.setRemarks("System Generated");
          augustWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          augustWithdrawal.setUser(user);

          svn = toAmount(line, 20);
          blc = blc + svn;
          Saving sepSaving = new Saving();
          sepSaving.setAmount(svn);
          sepSaving.setBalance(blc);
          sepSaving.setCreatedDate(Util.toPayDay(intYear, Month.SEPTEMBER));
          sepSaving.setRemarks("System Generated");
          sepSaving.setType(Saving.Type.NORMAL);
          sepSaving.setUser(user);

          wit = toAmount(line, 21);
          blc = blc - wit;
          Withdrawal sepWithdrawal = new Withdrawal();
          sepWithdrawal.setAmount(wit);
          sepWithdrawal.setBalance(blc);
          sepWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.SEPTEMBER));
          sepWithdrawal.setRemarks("System Generated");
          sepWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          sepWithdrawal.setUser(user);

          svn = toAmount(line, 22);
          blc = blc + svn;
          Saving octoSaving = new Saving();
          octoSaving.setAmount(svn);
          octoSaving.setBalance(blc);
          octoSaving.setCreatedDate(Util.toPayDay(intYear, Month.OCTOBER));
          octoSaving.setRemarks("System Generated");
          octoSaving.setType(Saving.Type.NORMAL);
          octoSaving.setUser(user);

          wit = toAmount(line, 23);
          blc = blc - wit;
          Withdrawal octoWithdrawal = new Withdrawal();
          octoWithdrawal.setAmount(wit);
          octoWithdrawal.setBalance(blc);
          octoWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.OCTOBER));
          octoWithdrawal.setRemarks("System Generated");
          octoWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          octoWithdrawal.setUser(user);

          svn = toAmount(line, 24);
          blc = blc + svn;
          Saving noveSaving = new Saving();
          noveSaving.setAmount(svn);
          noveSaving.setBalance(blc);
          noveSaving.setCreatedDate(Util.toPayDay(intYear, Month.NOVEMBER));
          noveSaving.setRemarks("System Generated");
          noveSaving.setType(Saving.Type.NORMAL);
          noveSaving.setUser(user);

          wit = toAmount(line, 25);
          blc = blc - wit;
          Withdrawal noveWithdrawal = new Withdrawal();
          noveWithdrawal.setAmount(wit);
          noveWithdrawal.setBalance(blc);
          noveWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.NOVEMBER));
          noveWithdrawal.setRemarks("System Generated");
          noveWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          noveWithdrawal.setUser(user);

          svn = toAmount(line, 26);
          blc = blc + svn;
          Saving decSaving = new Saving();
          decSaving.setAmount(svn);
          decSaving.setBalance(blc);
          decSaving.setCreatedDate(Util.toPayDay(intYear, Month.DECEMBER));
          decSaving.setRemarks("System Generated");
          decSaving.setType(Saving.Type.NORMAL);
          decSaving.setUser(user);

          wit = toAmount(line, 27);
          blc = blc - wit;
          Withdrawal decWithdrawal = new Withdrawal();
          decWithdrawal.setAmount(wit);
          decWithdrawal.setBalance(blc);
          decWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.DECEMBER));
          decWithdrawal.setRemarks("System Generated");
          decWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          decWithdrawal.setUser(user);

          Set<Saving> allSavings = new HashSet<>();
          allSavings.add(janSaving);
          allSavings.add(febSaving);
          allSavings.add(marchSaving);
          allSavings.add(aprilSaving);
          allSavings.add(maySaving);
          allSavings.add(juneSaving);
          allSavings.add(julySaving);
          allSavings.add(augustSaving);
          allSavings.add(sepSaving);
          allSavings.add(octoSaving);
          allSavings.add(noveSaving);
          allSavings.add(decSaving);

          Set<Withdrawal> allWithdrawals = new HashSet<>();
          allWithdrawals.add(janWithdrawal);
          allWithdrawals.add(febWithdrawal);
          allWithdrawals.add(marchWithdrawal);
          allWithdrawals.add(aprilWithdrawal);
          allWithdrawals.add(mayWithdrawal);
          allWithdrawals.add(juneWithdrawal);
          allWithdrawals.add(julyWithdrawal);
          allWithdrawals.add(augustWithdrawal);
          allWithdrawals.add(sepWithdrawal);
          allWithdrawals.add(octoWithdrawal);
          allWithdrawals.add(noveWithdrawal);
          allWithdrawals.add(decWithdrawal);

          Set<Balance> allBalances = new HashSet<>();
          balance.setUser(user);
          allBalances.add(balance);

          user.setSavings(allSavings);
          user.setWithdrawals(allWithdrawals);
          user.setBalances(allBalances);
          user.setPassword(passwordManager.hashPasword(extract(line, 1)));

          //persist user account
          System.out.println("processing data at " + count);
          if (!user.getIppisNumber().isEmpty()) {
            entityManager.persist(user);
          }
        }

      }
      count++;
    }
    System.out.println("Done loading 2016 Savings");
  }

  public static void loadS2017(String filePath, EntityManager entityManager, PasswordManager passwordManager) throws IOException {
    Iterator<String> mainData = new MMapFile(filePath).head();

    int count = 0;
    //Begin 2017 Data
    while (mainData.hasNext()) {
      String line = mainData.next();
      if (count == 0) {
        System.out.println(line);
      } else {
        if (!line.isEmpty()) {
          String stringYear = "2017";
          int intYear = 2017;

          double blc = toAmount(line, 3);
          double svn = 0.0;
          double wit = 0.0;

          double totalSavings = toAmount(line, 28);
          double totalwithdrawals = toAmount(line, 29);
          double closingBalance = toAmount(line, 30);

          Balance balance = new Balance();
          balance.setBalanceBefore(blc);
          balance.setClosingBalance(toAmount(line, 30));
          balance.setCurrentBalance(closingBalance);
          balance.setYear(stringYear);

          User user = new User();
          user.setIppisNumber(extract(line, 1));
          user.setName(extract(line, 2));
          user.setPhoneNumber(extract(line, 31));
          user.setBalanceBefore(blc);
          user.setEmail(extract(line, 32));
          user.setRole(User.Role.NORMAL);
          user.setContributionAmount(0.0);
          user.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));

          List<User> users = entityManager.createNamedQuery("User.findByIppisNumber", User.class)
              .setParameter("number", user.getIppisNumber())
              .getResultList();
          if (!users.isEmpty()) {
            user = users.get(0);
          }

          svn = toAmount(line, 4);
          blc = blc + svn;
          Saving janSaving = new Saving();
          janSaving.setAmount(svn);
          janSaving.setBalance(blc);
          janSaving.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));
          janSaving.setRemarks("System Generated");
          janSaving.setType(Saving.Type.NORMAL);
          janSaving.setUser(user);
          user.setContributionAmount(svn);

          wit = toAmount(line, 5);
          blc = blc - wit;
          Withdrawal janWithdrawal = new Withdrawal();
          janWithdrawal.setAmount(wit);
          janWithdrawal.setBalance(blc);
          janWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));
          janWithdrawal.setRemarks("System Generated");
          janWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          janWithdrawal.setUser(user);

          svn = toAmount(line, 6);
          blc = blc + svn;
          Saving febSaving = new Saving();
          febSaving.setAmount(svn);
          febSaving.setBalance(blc);
          febSaving.setCreatedDate(Util.toPayDay(intYear, Month.FEBRUARY));
          febSaving.setRemarks("System Generated");
          febSaving.setType(Saving.Type.NORMAL);
          febSaving.setUser(user);

          wit = toAmount(line, 7);
          blc = blc - wit;
          Withdrawal febWithdrawal = new Withdrawal();
          febWithdrawal.setAmount(wit);
          febWithdrawal.setBalance(blc);
          febWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.FEBRUARY));
          febWithdrawal.setRemarks("System Generated");
          febWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          febWithdrawal.setUser(user);

          svn = toAmount(line, 8);
          blc = blc + svn;
          Saving marchSaving = new Saving();
          marchSaving.setAmount(svn);
          marchSaving.setBalance(blc);
          marchSaving.setCreatedDate(Util.toPayDay(intYear, Month.MARCH));
          marchSaving.setRemarks("System Generated");
          marchSaving.setType(Saving.Type.NORMAL);
          marchSaving.setUser(user);

          wit = toAmount(line, 9);
          blc = blc - wit;
          Withdrawal marchWithdrawal = new Withdrawal();
          marchWithdrawal.setAmount(wit);
          marchWithdrawal.setBalance(blc);
          marchWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.MARCH));
          marchWithdrawal.setRemarks("System Generated");
          marchWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          marchWithdrawal.setUser(user);

          svn = toAmount(line, 10);
          blc = blc + svn;
          Saving aprilSaving = new Saving();
          aprilSaving.setAmount(svn);
          aprilSaving.setBalance(blc);
          aprilSaving.setCreatedDate(Util.toPayDay(intYear, Month.APRIL));
          aprilSaving.setRemarks("System Generated");
          aprilSaving.setType(Saving.Type.NORMAL);
          aprilSaving.setUser(user);

          wit = toAmount(line, 11);
          blc = blc - wit;
          Withdrawal aprilWithdrawal = new Withdrawal();
          aprilWithdrawal.setAmount(wit);
          aprilWithdrawal.setBalance(blc);
          aprilWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.APRIL));
          aprilWithdrawal.setRemarks("System Generated");
          aprilWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          aprilWithdrawal.setUser(user);

          svn = toAmount(line, 12);
          blc = blc + svn;
          Saving maySaving = new Saving();
          maySaving.setAmount(svn);
          maySaving.setBalance(blc);
          maySaving.setCreatedDate(Util.toPayDay(intYear, Month.MAY));
          maySaving.setRemarks("System Generated");
          maySaving.setType(Saving.Type.NORMAL);
          maySaving.setUser(user);

          wit = toAmount(line, 13);
          blc = blc - wit;
          Withdrawal mayWithdrawal = new Withdrawal();
          mayWithdrawal.setAmount(wit);
          mayWithdrawal.setBalance(blc);
          mayWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.MAY));
          mayWithdrawal.setRemarks("System Generated");
          mayWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          mayWithdrawal.setUser(user);

          svn = toAmount(line, 14);
          blc = blc + svn;
          Saving juneSaving = new Saving();
          juneSaving.setAmount(svn);
          juneSaving.setBalance(blc);
          juneSaving.setCreatedDate(Util.toPayDay(intYear, Month.JUNE));
          juneSaving.setRemarks("System Generated");
          juneSaving.setType(Saving.Type.NORMAL);
          juneSaving.setUser(user);

          wit = toAmount(line, 15);
          blc = blc - wit;
          Withdrawal juneWithdrawal = new Withdrawal();
          juneWithdrawal.setAmount(wit);
          juneWithdrawal.setBalance(blc);
          juneWithdrawal.setCreatedDate(Util.toPayDay(intYear, Month.JUNE));
          juneWithdrawal.setRemarks("System Generated");
          juneWithdrawal.setStatus(Withdrawal.Status.APPROVED);
          juneWithdrawal.setUser(user);

          Set<Saving> allSavings = new HashSet<>();
          allSavings.add(janSaving);
          allSavings.add(febSaving);
          allSavings.add(marchSaving);
          allSavings.add(aprilSaving);
          allSavings.add(maySaving);
          allSavings.add(juneSaving);

          Set<Withdrawal> allWithdrawals = new HashSet<>();
          allWithdrawals.add(janWithdrawal);
          allWithdrawals.add(febWithdrawal);
          allWithdrawals.add(marchWithdrawal);
          allWithdrawals.add(aprilWithdrawal);
          allWithdrawals.add(mayWithdrawal);
          allWithdrawals.add(juneWithdrawal);

          Set<Balance> allBalances = new HashSet<>();
          balance.setUser(user);
          allBalances.add(balance);

          Set<Saving> savings = user.getSavings();
          if (Objects.isNull(savings) || savings.isEmpty()) {
            user.setSavings(allSavings);
          } else {
            user.getSavings().addAll(allSavings);
          }

          Set<Withdrawal> withdrawals = user.getWithdrawals();
          if (Objects.isNull(withdrawals) || withdrawals.isEmpty()) {
            user.setWithdrawals(withdrawals);
          } else {
            user.getWithdrawals().addAll(allWithdrawals);
          }
          Set<Balance> balances = user.getBalances();
          if (Objects.isNull(balances) || balances.isEmpty()) {
            user.setBalances(balances);
          } else {
            user.getBalances().addAll(allBalances);
          }
          user.setPassword(passwordManager.hashPasword(extract(line, 1)));

          //persist user account
          System.out.println("processing data at " + count);
          user.setContributionAmount(juneSaving.getAmount());
          if (!user.getIppisNumber().isEmpty()) {
            entityManager.merge(user);
          }
        }

      }
      count++;
    }
    System.out.println("Done loading 2017 savings");
  }

  public static void loadL2017(String filePath, EntityManager entityManager, PasswordManager passwordManager) throws IOException {
    Iterator<String> mainData = new MMapFile(filePath).head();

    int count = 0;
    //Begin 2016 Data
    while (mainData.hasNext()) {
      String line = mainData.next();
      if (count == 0) {
      } else {
        if (!line.isEmpty()) {
          String stringYear = "2017";
          int intYear = 2017;

          double loanbefore = toAmount(line, 3);
          double deductions = 0.0;

          double totalLoan = toAmount(line, 29);
          double totalDeductionLoan = toAmount(line, 30);
          double loanBalance = toAmount(line, 31);

          double blc = toAmount(line, 3);
          double lp = 0.0;
          double l = 0.0;

          User user = new User();
          user.setIppisNumber(extract(line, 1));
          user.setName(extract(line, 2));
          user.setPhoneNumber(extract(line, 31));
          user.setBalanceBefore(blc);
          user.setEmail(extract(line, 32));
          user.setRole(User.Role.NORMAL);
          user.setContributionAmount(0.0);
          user.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));

          List<User> users = entityManager
              .createNamedQuery("User.findByIppisNumber", User.class)
              .setParameter("number", user.getIppisNumber())
              .getResultList();
          if (!users.isEmpty()) {
            user = users.get(0);
          }

          List<Loan> gbese = new ArrayList<>();

          Loan specialLoan = new Loan();
          specialLoan.setAmount(toAmount(line, 4));
          specialLoan.setApproval(Loan.Approval.APPROVED);
          specialLoan.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));
          specialLoan.setNumberOfMonths(0);
          specialLoan.setPaid(0.0);
          specialLoan.setRepayment(0.0);
          specialLoan.setSkipMonths(0);
          specialLoan.setSkipped(0);
          specialLoan.setStatus(Loan.Status.UNCLEAR);
          specialLoan.setType(Loan.Type.SPECIAL);
          specialLoan.setUser(user);

          lp = toAmount(line, 5);
          LoanPaymentHistory janDecPayment = new LoanPaymentHistory();
          janDecPayment.setAmount(lp);
          janDecPayment.setDate(Util.toPayDay(intYear, Month.JANUARY));

          l = toAmount(line, 6) + toAmount(line, 3);
          Loan janLoan = new Loan();
          janLoan.setAmount(l);
          janLoan.setApproval(Loan.Approval.APPROVED);
          janLoan.setCreatedDate(Util.toPayDay(intYear, Month.JANUARY));
          janLoan.setNumberOfMonths(0);
          janLoan.setSkipMonths(0);
          janLoan.setSkipped(0);
          janLoan.setStatus(Loan.Status.UNCLEAR);
          janLoan.setType(Loan.Type.REGULAR);
          janLoan.setUser(user);
          addLoan(gbese, janLoan, janDecPayment);

          lp = toAmount(line, 7);
          LoanPaymentHistory febDecPayment = new LoanPaymentHistory();
          febDecPayment.setAmount(lp);
          febDecPayment.setDate(Util.toPayDay(intYear, Month.FEBRUARY));

          l = toAmount(line, 8);
          Loan febLoan = new Loan();
          febLoan.setAmount(l);
          febLoan.setApproval(Loan.Approval.APPROVED);
          febLoan.setCreatedDate(Util.toPayDay(intYear, Month.FEBRUARY));
          febLoan.setNumberOfMonths(0);
          febLoan.setPaid(0.0);
          febLoan.setRepayment(0.0);
          febLoan.setSkipMonths(0);
          febLoan.setSkipped(0);
          febLoan.setStatus(Loan.Status.UNCLEAR);
          febLoan.setType(Loan.Type.REGULAR);
          febLoan.setUser(user);
          addLoan(gbese, febLoan, febDecPayment);

          lp = toAmount(line, 9);
          LoanPaymentHistory marchDecPayment = new LoanPaymentHistory();
          marchDecPayment.setAmount(lp);
          marchDecPayment.setDate(Util.toPayDay(intYear, Month.MARCH));

          l = toAmount(line, 10);
          Loan marchLoan = new Loan();
          marchLoan.setAmount(l);
          marchLoan.setApproval(Loan.Approval.APPROVED);
          marchLoan.setCreatedDate(Util.toPayDay(intYear, Month.MARCH));
          marchLoan.setNumberOfMonths(0);
          marchLoan.setPaid(0.0);
          marchLoan.setRepayment(0.0);
          marchLoan.setSkipMonths(0);
          marchLoan.setSkipped(0);
          marchLoan.setStatus(Loan.Status.UNCLEAR);
          marchLoan.setType(Loan.Type.REGULAR);
          marchLoan.setUser(user);
          addLoan(gbese, marchLoan, marchDecPayment);

          lp = toAmount(line, 11);
          LoanPaymentHistory aprilDecPayment = new LoanPaymentHistory();
          aprilDecPayment.setAmount(lp);
          aprilDecPayment.setDate(Util.toPayDay(intYear, Month.APRIL));

          l = toAmount(line, 12);
          Loan aprilLoan = new Loan();
          aprilLoan.setAmount(l);
          aprilLoan.setApproval(Loan.Approval.APPROVED);
          aprilLoan.setCreatedDate(Util.toPayDay(intYear, Month.APRIL));
          aprilLoan.setNumberOfMonths(0);
          aprilLoan.setPaid(0.0);
          aprilLoan.setRepayment(0.0);
          aprilLoan.setSkipMonths(0);
          aprilLoan.setSkipped(0);
          aprilLoan.setStatus(Loan.Status.UNCLEAR);
          aprilLoan.setType(Loan.Type.REGULAR);
          aprilLoan.setUser(user);
          addLoan(gbese, aprilLoan, aprilDecPayment);

          lp = toAmount(line, 13);
          LoanPaymentHistory mayDecPayment = new LoanPaymentHistory();
          mayDecPayment.setAmount(lp);
          mayDecPayment.setDate(Util.toPayDay(intYear, Month.MAY));

          l = toAmount(line, 14);
          Loan mayLoan = new Loan();
          mayLoan.setAmount(l);
          mayLoan.setApproval(Loan.Approval.APPROVED);
          mayLoan.setCreatedDate(Util.toPayDay(intYear, Month.MAY));
          mayLoan.setNumberOfMonths(0);
          mayLoan.setPaid(0.0);
          mayLoan.setRepayment(0.0);
          mayLoan.setSkipMonths(0);
          mayLoan.setSkipped(0);
          mayLoan.setStatus(Loan.Status.UNCLEAR);
          mayLoan.setType(Loan.Type.REGULAR);
          mayLoan.setUser(user);
          addLoan(gbese, mayLoan, mayDecPayment);

          lp = toAmount(line, 15);
          LoanPaymentHistory juneDecPayment = new LoanPaymentHistory();
          juneDecPayment.setAmount(lp);
          juneDecPayment.setDate(Util.toPayDay(intYear, Month.JUNE));

          l = toAmount(line, 16);
          Loan juneLoan = new Loan();
          juneLoan.setAmount(l);
          juneLoan.setApproval(Loan.Approval.APPROVED);
          juneLoan.setCreatedDate(Util.toPayDay(intYear, Month.JUNE));
          juneLoan.setNumberOfMonths(0);
          juneLoan.setPaid(0.0);
          juneLoan.setRepayment(0.0);
          juneLoan.setSkipMonths(0);
          juneLoan.setSkipped(0);
          juneLoan.setStatus(Loan.Status.UNCLEAR);
          juneLoan.setType(Loan.Type.REGULAR);
          juneLoan.setUser(user);
          addLoan(gbese, juneLoan, juneDecPayment);

          List<Balance> balances = entityManager
              .createNamedQuery("Balance.findByYear", Balance.class)
              .setParameter("ippis", user.getIppisNumber())
              .setParameter("year", stringYear)
              .getResultList();

          Balance balance = null;
          if (Objects.isNull(balances) || balances.isEmpty()) {
            balance = new Balance();
          } else {
            balance = balances.get(0);
          }

          balance.setTotalLoan(totalLoan);
          balance.setYear(stringYear);
          balance.setLoanBefore(loanbefore);
          balance.setPaidLoan(totalDeductionLoan);
          balance.setUnclearedLoan(loanBalance);
          balance.setYear(stringYear);
          balance.setUser(user);

          Set<Loan> loans = user.getLoans();
          if (Objects.isNull(loans) || loans.isEmpty()) {
            loans = new HashSet<>();
            loans.add(specialLoan);
            loans.addAll(gbese);
            user.setLoans(loans);
          } else {
            user.getLoans().add(specialLoan);
            user.getLoans().addAll(gbese);
          }

          
          if (!user.getIppisNumber().isEmpty()) {
            entityManager.merge(user);
            entityManager.merge(balance);
          }
          System.out.println("Processing loans for " + user.getId());
        }
      }
      count++;
    }
    System.out.println("Done processing loans");
  }

  private static String extract(String line, int col) {
    String[] split = line.split(DELIMETER);
    try {
      return clean(split[col]);
    } catch (ArrayIndexOutOfBoundsException aioobe) {
      return "";
    }
  }

  private static String clean(String input) {
    if (Objects.isNull(input) || input.isEmpty()) {
      return "";
    } else {
      return input.replaceAll("\"", "").replace("(", "").replace(")", "").trim();
    }
  }

  private static Double toDouble(String input) {
    if (Objects.isNull(input) || input.isEmpty()) {
      return 0.00;
    } else {
      try {
        return Double.parseDouble(input.replaceAll(",", "").replaceAll("-", "").trim());
      } catch (NumberFormatException nfe) {
        return 0.00;
      }
    }
  }

  private static Double toAmount(String line, int col) {
    return toDouble(extract(line, col));
  }

  public static Loan getLastLoan(List<Loan> loans) {
    if (loans.isEmpty()) {
      return null;
    } else {
      return loans.get(loans.size() - 1);
    }
  }

  public static void updateRepayment(Loan loan, LoanPaymentHistory loanPayment) {
    if (Objects.nonNull(loan) && Objects.nonNull(loanPayment)) {
      loan.setRepayment(loanPayment.getAmount());
      if (Objects.nonNull(loan.getPaid())) {
        loan.setPaid(loan.getPaid() + loanPayment.getAmount());
        loanPayment.setBalance(loan.getAmount() - loan.getPaid());
      } else {
        loan.setPaid(loanPayment.getAmount());
        loanPayment.setBalance(loan.getAmount() - loan.getPaid());
      }
      if (loan.getPaid() >= loan.getAmount()) {
        loan.setStatus(Loan.Status.CLEARED);
      }
      loanPayment.setLoan(loan);
      Set<LoanPaymentHistory> loanPayments = loan.getLoanPayments();
      if (Objects.isNull(loanPayments) || loanPayments.isEmpty()) {
        loanPayments = new HashSet<>();
        loanPayments.add(loanPayment);
        loan.setLoanPayments(loanPayments);
      } else {
        loanPayments.add(loanPayment);
      }
      
    }
  }

  public static void addLoan(List<Loan> loans, Loan loan, LoanPaymentHistory loanPayment) {
    if (Objects.nonNull(loan) && (loan.getAmount() > 0)) {
      loans.add(loan);
    }
    Loan lastLoan = getLastLoan(loans);
    updateRepayment(lastLoan, loanPayment);
  }
}
