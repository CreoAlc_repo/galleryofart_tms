/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author prodigy4440
 */
public class Excel {

    private static <T> String getHeader(T obj, int... exclude) {
        StringBuilder builder = new StringBuilder();
        Inflector inflector = Inflector.getInstance();
        List<Integer> excludes = new LinkedList<>();
        for (Integer in : exclude) {
            excludes.add(in);
        }

        String fieldNames = ObjectUtils.getFieldNames(obj);
        String[] fields = fieldNames.split(",");

        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            if (!excludes.contains(i)) {
                if (builder.length() != 0) {
                    builder.append(",");
                }
                builder.append(inflector.titleCase(inflector.splitCamelCase(field)));
            }
        }

        return builder.toString();
    }

    private static <T> String getValues(T object, int... exclude) {
        StringBuilder builder = new StringBuilder();
        List<Integer> excludes = new LinkedList<>();
        for (Integer in : exclude) {
            excludes.add(in);
        }

        String[] values = ObjectUtils.getFieldValues(object).split(",");
        for (int i = 0; i < values.length; i++) {

            if (!excludes.contains(i)) {
                String value = values[i];
                if (builder.length() != 0) {
                    builder.append(",");
                }
                builder.append(value);

            }

        }

        return builder.toString();
    }


    private static void write(String data, Row row) {
        String[] split = data.split(",");
        for (int i = 0; i < split.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(split[i]);
        }
    }
    
    public static <T> String exportExcel(List<T> data, int... exclude) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        
        if(data.size() == 0){
            return "";
        }else{
            T firstData = data.get(0);
            write(getHeader(firstData,exclude), sheet.createRow(0));
            int rowCount = 0;
            for (Object object : data) {
                Row row = sheet.createRow(++rowCount);
                write(getValues(object, exclude), row);
            }
        }
        
        Path filePath = Files.createTempFile("ajo", ".xls");
        try (FileOutputStream outputStream = new FileOutputStream(filePath.toFile())) {
            workbook.write(outputStream);
        }
        return filePath.toString();
    }
    
}
