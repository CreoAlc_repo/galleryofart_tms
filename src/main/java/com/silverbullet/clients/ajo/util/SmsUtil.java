/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo.util;

import java.io.IOException;
import java.util.Objects;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author prodigy4440
 */
public class SmsUtil {

    private String SUBACCT = "CREOALC";
    private String SUBACCTPWD = "R37qs!yT2i!w";
    private String OWNER_EMAIL = "creoalc@gmail.com";
    private String SESSION_ID = "";

    public Status login()
            throws IOException {
        String ownerEmail = OWNER_EMAIL;
        String subAcct = SUBACCT;
        String subAccPwd = SUBACCTPWD;

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host("www.smslive247.com")
                .addPathSegment("http")
                .addPathSegment("index.aspx")
                .addQueryParameter("cmd", "login")
                .addQueryParameter("owneremail", ownerEmail)
                .addQueryParameter("subacct", subAcct)
                .addQueryParameter("subacctpwd", subAccPwd)
                .build();

        Request request = new Request.Builder()
                .url(httpUrl)
                .get()
                .build();

        OkHttpClient httpClient = new OkHttpClient();
        Response response = httpClient.newCall(request).execute();
        String[] split = response.body().string().split(":");
        String desc = split[0];
        String data = split[split.length - 1];

        if (desc.equalsIgnoreCase("OK")) {
            return new Status(0, desc, data.trim());
        } else {
            return new Status(9, data, null);
        }
    }

    public Status sendSms(String message, String sender, String sendTo)
            throws IOException {

        Status login = login();
        if (login.getCode() == 0) {
            SESSION_ID = login.getData();
        }

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host("www.smslive247.com")
                .addPathSegment("http")
                .addPathSegment("index.aspx")
                .addQueryParameter("cmd", "sendmsg")
                .addQueryParameter("sessionid", SESSION_ID)
                .addQueryParameter("message", message)
                .addQueryParameter("sender", sender)
                .addQueryParameter("sendto", sendTo)
                .addQueryParameter("msgtype", "0")
                .build();

        Request request = new Request.Builder()
                .url(httpUrl)
                .get()
                .build();

        OkHttpClient httpClient = new OkHttpClient();
        Response response = httpClient.newCall(request).execute();
        String[] split = response.body().string().split(":");
        String desc = split[0];
        String data = split[split.length - 1];

        if (desc.equalsIgnoreCase("OK")) {
            return new Status(0, desc, data.trim());
        } else {
            return new Status(9, data, null);
        }
    }

    public class Status {

        //0 success
        //9 Error
        private int code;
        private String description;
        private String data;

        public Status() {
        }

        public Status(int code, String description, String data) {
            this.code = code;
            this.description = description;
            this.data = data;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 53 * hash + this.code;
            hash = 53 * hash + Objects.hashCode(this.description);
            hash = 53 * hash + Objects.hashCode(this.data);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Status other = (Status) obj;
            if (this.code != other.code) {
                return false;
            }
            if (!Objects.equals(this.description, other.description)) {
                return false;
            }
            if (!Objects.equals(this.data, other.data)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "Status{" + "code=" + code + ", description=" + description + ", data=" + data + '}';
        }

    }
}
