/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo;

import com.silverbullet.clients.ajo.entities.Balance;
import com.silverbullet.clients.ajo.entities.Loan;
import com.silverbullet.clients.ajo.entities.LoanPaymentHistory;
import com.silverbullet.clients.ajo.entities.Saving;
import com.silverbullet.clients.ajo.entities.User;
import com.silverbullet.clients.ajo.util.Util;
import com.silverbullet.gaora.ejb.ConcurrencyManager;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class RunnerService {

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private ConcurrencyManager concurrencyManager;

    @EJB
    private SmsManager smsManager;

    @Schedule(year = "*", month = "*", dayOfMonth = "7", hour = "9", persistent = false)
    public void monthlySavingSchedule() {
        List<User> users = entityManager.createQuery("SELECT u FROM User u", User.class)
                .getResultList();
        for (User user : users) {
            Double amount = user.getContributionAmount();
            if (Objects.isNull(amount)) {
                amount = 0.0;
            }

            List<Balance> balances = entityManager
                    .createNamedQuery("Balance.currentYear", Balance.class)
                    .setParameter("ippis", user.getIppisNumber())
                    .getResultList();

            Balance balance = new Balance(0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    Util.getCurrentYear(), user);
            if (!balances.isEmpty()) {
                balance = balances.get(0);
            }

            if (Objects.isNull(balance.getCurrentBalance())) {
                balance.setCurrentBalance(0.0);
            }

            balance.setCurrentBalance(balance.getCurrentBalance() + amount);

            Saving saving = new Saving();
            saving.setAmount(user.getContributionAmount());
            saving.setBalance(balance.getCurrentBalance());
            saving.setCreatedDate(Util.toPayDay(Year.now().getValue(),
                    Month.from(LocalDate.now().minusMonths(1))));
            saving.setRemarks("System Generated");
            saving.setType(Saving.Type.NORMAL);
            saving.setUser(user);
            Set<Saving> savings = user.getSavings();
            if (Objects.isNull(savings) || savings.isEmpty()) {
                savings = new HashSet<>();
                savings.add(saving);
                user.setSavings(savings);
            } else {
                user.getSavings().add(saving);
            }
            entityManager.merge(balance);
            entityManager.merge(user);

            String sms = "Your Coop. A/C with IPPIS " + user.getIppisNumber()
                    + " has been credited with N" + saving.getAmount() + " for the month of "
                    + Month.from(LocalDate.now().minusMonths(1))
                    + "," + Util.getCurrentYear() + ". Bal. N" + balance.getCurrentBalance()
                    + "(Savings).";

            smsManager.sendSms(sms, user.getPhoneNumber());
        }
    }

    @Schedule(year = "*", month = "*", dayOfMonth = "7", hour = "10", persistent = false)
    public void monthlyLoanSchedule() {
        List<User> users = entityManager.createQuery("SELECT u FROM User u", User.class)
                .getResultList();
        for (User user : users) {
            List<Balance> balances = entityManager
                    .createNamedQuery("Balance.currentYear", Balance.class)
                    .setParameter("ippis", user.getIppisNumber())
                    .getResultList();

            Balance balance = new Balance(0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    Util.getCurrentYear(), user);
            if (!balances.isEmpty()) {
                balance = balances.get(0);
            }

            List<Loan> loans = entityManager.createNamedQuery("Loan.findForRepaymentUsingIppis", Loan.class)
                    .setParameter("ippis", user.getIppisNumber())
                    .setParameter("approval", Loan.Approval.APPROVED)
                    .setParameter("status", Loan.Status.CLEARED)
                    .setMaxResults(1)
                    .getResultList();
            if (!loans.isEmpty()) {
                Loan loan = loans.get(0);
                Double amount = loan.getRepayment();
                Double outstanding = loan.getAmount() - loan.getPaid();
                LoanPaymentHistory loanPaymentHistory = new LoanPaymentHistory();
                loanPaymentHistory.setDate(Util.toPayDay(Year.now().getValue(),
                        Month.from(LocalDate.now().minusMonths(1))));
                loanPaymentHistory.setLoan(loan);
                if (outstanding <= amount) {
                    amount = outstanding;
                    loanPaymentHistory.setAmount(outstanding);
                    loan.setPaid(loan.getPaid() + outstanding);
                    loan.setStatus(Loan.Status.CLEARED);
                    balance.setPaidLoan(balance.getPaidLoan() + outstanding);
                    balance.setUnclearedLoan(balance.getUnclearedLoan() - outstanding);
                } else {
                    loanPaymentHistory.setAmount(amount);
                    loan.setPaid(loan.getPaid() + amount);
                    balance.setPaidLoan(balance.getPaidLoan() + amount);
                    balance.setUnclearedLoan(balance.getUnclearedLoan() - amount);
                }
                loanPaymentHistory.setBalance(loan.getAmount() - loan.getPaid());
                Set<LoanPaymentHistory> loanPayments = loan.getLoanPayments();
                if (Objects.isNull(loanPayments)) {
                    loanPayments = new HashSet<>();
                    loanPayments.add(loanPaymentHistory);
                    loan.setLoanPayments(loanPayments);
                } else {
                    loanPayments.add(loanPaymentHistory);
                }
                entityManager.merge(balance);
                entityManager.merge(loan);

                if (Objects.equals(amount, outstanding)) {
                    outstanding = 0.0;
                }
                String sms = "Your Coop. Loan A/c with IPPIS " + user.getIppisNumber()
                        + " has been debited with N" + amount + " for the month of "
                        + Month.from(LocalDate.now().minusMonths(1)) + " 2017\n"
                        + "Outstanding Bal. N" + outstanding + " (Loan)";
                smsManager.sendSms(sms, user.getPhoneNumber());
            }

        }
    }

}
