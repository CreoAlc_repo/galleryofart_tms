/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.ajo;

import com.silverbullet.clients.ajo.entities.Activity;
import com.silverbullet.clients.ajo.entities.Balance;
import com.silverbullet.clients.ajo.entities.Loan;
import com.silverbullet.clients.ajo.entities.LoanPaymentHistory;
import com.silverbullet.clients.ajo.entities.Saving;
import com.silverbullet.clients.ajo.entities.User;
import com.silverbullet.clients.ajo.entities.Withdrawal;
import com.silverbullet.clients.ajo.request.LoanBean;
import com.silverbullet.clients.ajo.request.RepaymentBean;
import com.silverbullet.clients.ajo.request.UserBean;
import com.silverbullet.clients.ajo.util.Constant;
import com.silverbullet.clients.ajo.util.Csv;
import com.silverbullet.clients.ajo.util.ExportData;
import com.silverbullet.clients.ajo.util.Util;
import com.silverbullet.gaora.ejb.CacheManager;
import com.silverbullet.gaora.ejb.PasswordManager;
import com.silverbullet.gaora.entities.Auth;
import com.silverbullet.gaora.request.LoginBean;
import com.silverbullet.gaora.responses.BaseResponse;
import com.silverbullet.gaora.responses.ResponseCodes;
import com.silverbullet.gaora.responses.ResponseUtil;
import com.silverbullet.gaora.util.MessageLogger;
import java.io.IOException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class Service {

    @EJB
    private CacheManager<String, Auth<String, User>> cacheManager;

    @EJB
    private PasswordManager passwordManager;

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private SmsManager smsManager;

    public BaseResponse createUser(User adminUser, UserBean userBean) {

        if (Objects.isNull(userBean)) {
            return ResponseUtil.badInputParam("User information not supplied");
        }

        if (Objects.isNull(userBean.getIppis()) || userBean.getIppis().isEmpty()
                || Objects.isNull(userBean.getPhone()) || userBean.getPhone().isEmpty()) {
            return ResponseUtil.badInputParam("User information not complete");
        }

        User user = new User();
        user.setCreatedDate(new Date());
        user.setEmail(userBean.getEmail());
        user.setIppisNumber(userBean.getIppis());
        user.setName(userBean.getName());
        user.setPassword(passwordManager.hashPasword(userBean.getIppis()));
        user.setPhoneNumber(userBean.getPhone());
        user.setContributionAmount(userBean.getAmount());
        user.setRole(userBean.getRole());
        user.setBalanceBefore(0.0);

        Balance balance = new Balance(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                Util.getCurrentYear(), user);
        Set<Balance> balances = new HashSet<>();
        balances.add(balance);
        user.setBalances(balances);

        List<User> users = entityManager.createNamedQuery("User.findByIppisNumber",
                User.class).setParameter("number", userBean.getIppis())
                .getResultList();
        if (users.isEmpty()) {
            entityManager.persist(user);

            Activity activity = new Activity();
            activity.setCreatedDate(new Date());
            activity.setActivity("Account Creation");
            activity.setPerformedBy(adminUser.getIppisNumber()
                    + " - " + adminUser.getName());
            activity.setPerformedOn(user.getIppisNumber()
                    + " - " + user.getName());
            activity.setPhoneNumber(user.getPhoneNumber());
            activity.setRole(adminUser.getRole().name());
            entityManager.persist(activity);

            smsManager.sendSms("Coop Account has been created for you,"
                    + " IPPIS is " + user.getIppisNumber()
                    + ", your defaul password is the same as your IPPIS",
                    user.getPhoneNumber());

            return ResponseUtil.success("Account created successfully", user);
        } else {
            return ResponseUtil.badInputParam("Ippis number already "
                    + "assigned to another person.");
        }

    }

    public BaseResponse loginUser(LoginBean loginBean) {

        List<User> users = entityManager.createNamedQuery("User.findByIppisNumber",
                User.class).setParameter("number", loginBean.getUsername())
                .getResultList();
        if (users.isEmpty()) {
            return ResponseUtil.recordNotFound("Account not found");
        } else {
            User user = users.get(0);
            if (passwordManager.checkPassword(loginBean.getPassword(), user.getPassword())) {

                String token = passwordManager.generatePassword(64);
                Auth<String, User> auth = new Auth<>();
                auth.setToken(token);
                auth.setV(user);
                cacheManager.put(token, auth);
                return ResponseUtil.success("User logged in successful", token);
            } else {
                return ResponseUtil.respond(ResponseCodes.PASSWORD_MISMATCH,
                        "Wrong password", null);
            }
        }
    }

    public BaseResponse getInfo(User user, String ippis) {

        List<User> users = entityManager.createNamedQuery("User.findByIppisNumber",
                User.class).setParameter("number", ippis)
                .getResultList();
        if (users.isEmpty()) {
            return ResponseUtil.recordNotFound("Invalid ippis number");
        } else {
            User get = users.get(0);
            return ResponseUtil.success("User fetched successful", get);
        }
    }

    public BaseResponse changePassword(User user, String newPassword,
            String confirmPassword) {
        if (Objects.isNull(newPassword) || newPassword.isEmpty()
                || Objects.isNull(confirmPassword) || confirmPassword.isEmpty()
                || (!Objects.deepEquals(newPassword, confirmPassword))) {
            return ResponseUtil.badInputParam("Empty or password does not match");
        } else {
            user.setPassword(passwordManager.hashPasword(newPassword));
            entityManager.merge(user);
            return ResponseUtil.success("Password updated", null);
        }
    }

    public BaseResponse resetPassword(User user, String ippis, String password) {
        if (user.getRole() == User.Role.NORMAL) {
            return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                    "User cannot reset password for user", null);
        } else {
            List<User> users = entityManager.createNamedQuery("User.findByIppisNumber",
                    User.class).setParameter("number", ippis)
                    .getResultList();
            if (users.isEmpty()) {
                return ResponseUtil.recordNotFound("Invalid ippis number");
            } else {
                User get = users.get(0);
                get.setPassword(passwordManager.hashPasword(password));
                entityManager.merge(get);

                Activity activity = new Activity();
                activity.setCreatedDate(new Date());
                activity.setActivity("Password reset");
                activity.setPerformedBy(user.getIppisNumber() + " - " + user.getName());
                activity.setPerformedOn(get.getIppisNumber() + " - " + get.getName());
                activity.setPhoneNumber(user.getPhoneNumber());
                activity.setRole(user.getRole().name());
                entityManager.persist(activity);
                return ResponseUtil.success("Password reset successful", null);
            }
        }
    }

    public BaseResponse createSavings(User user, String ippis, Double amount) {

        if (Objects.isNull(ippis) || Objects.isNull(amount)
                || (amount < 1)) {
            return ResponseUtil.badInputParam("User and or amount not specify");
        } else {
            if (user.getRole() == User.Role.ADMIN_1) {

                List<User> users = entityManager
                        .createNamedQuery("User.findByIppisNumber", User.class)
                        .setParameter("number", ippis).getResultList();
                if (users.isEmpty()) {
                    return ResponseUtil.recordNotFound("Invalide ippis number");
                } else {
                    User contribeUser = users.get(0);
                    Saving saving = new Saving(amount, "Special savings",
                            Saving.Type.SPECIAL, new Date(),
                            contribeUser);

                    List<Balance> balances = entityManager
                            .createNamedQuery("Balance.currentYear", Balance.class)
                            .setParameter("ippis", contribeUser.getIppisNumber())
                            .getResultList();
                    Balance balance = null;
                    if (balances.isEmpty()) {
                        balance = new Balance(0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                Util.getCurrentYear(), contribeUser);
                    } else {
                        balance = balances.get(0);
                    }

                    balance.setCurrentBalance(balance.getCurrentBalance() + amount);
                    saving.setBalance(balance.getCurrentBalance());
                    entityManager.merge(saving);
                    entityManager.merge(balance);

                    Activity activity = new Activity();
                    activity.setCreatedDate(new Date());
                    activity.setActivity("Create Special Savings");
                    activity.setPerformedBy(user.getIppisNumber() + " - " + user.getName());
                    activity.setPerformedOn(contribeUser.getIppisNumber()
                            + " - " + contribeUser.getName());
                    activity.setPhoneNumber(user.getPhoneNumber());
                    activity.setRole(user.getRole().name());
                    entityManager.persist(activity);

                    String sms = "Your Coop. A/C with IPPIS " + contribeUser.getIppisNumber()
                            + " has been credited with N" + saving.getAmount() + " for the month of "
                            + Util.getMonthFromDate(saving.getCreatedDate())
                            + "," + Util.getCurrentYear() + ". Bal. N" + balance.getCurrentBalance()
                            + "(Savings).";

                    smsManager.sendSms(sms, contribeUser.getPhoneNumber());

                    return ResponseUtil.success("Savings added successfully", null);
                }
            } else {
                return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                        "User does not have permission to perform operation", null);
            }
        }
    }

    public BaseResponse createLoan(User user, LoanBean loanBean) {

        if (Objects.isNull(user) || Objects.isNull(loanBean)) {
            return ResponseUtil.badInputParam("User not logged in or bad payload");
        } else {

            if (Objects.isNull(loanBean.getAmount()) || Objects.isNull(loanBean.getIppis())
                    || Objects.isNull(loanBean.getRepayment())) {
                return ResponseUtil.badInputParam(
                        "Amount, ippis and or repayment not specified");
            } else {

                if (user.getRole() == User.Role.ADMIN_1) {

                    List<User> users = entityManager
                            .createNamedQuery("User.findByIppisNumber", User.class)
                            .setParameter("number", loanBean.getIppis()).getResultList();
                    if (users.isEmpty()) {
                        return ResponseUtil.recordNotFound("Invalide ippis number");
                    } else {
                        User contribeUser = users.get(0);

                        Double amount = loanBean.getAmount();
                        Double repayment = loanBean.getRepayment();
                        Loan.Type type = Objects.isNull(loanBean.getType())
                                ? Loan.Type.REGULAR : loanBean.getType();

                        int months = (int) Math.ceil(amount / repayment);

                        Loan loan = new Loan();
                        loan.setAmount(amount);
                        loan.setCreatedDate(new Date());
                        loan.setNumberOfMonths(months);
                        loan.setPaid(0.0);
                        loan.setApproval(Loan.Approval.PENDING);
                        loan.setRepayment(repayment);
                        loan.setSkipMonths(0);
                        loan.setSkipped(0);
                        loan.setStatus(Loan.Status.UNCLEAR);
                        loan.setType(type);
                        loan.setUser(contribeUser);

                        if (Objects.isNull(contribeUser.getLoans())
                                || contribeUser.getLoans().isEmpty()) {
                            Set<Loan> loans = new HashSet<>();
                            loans.add(loan);
                            contribeUser.setLoans(loans);
                        } else {
                            contribeUser.getLoans().add(loan);
                        }

                        entityManager.merge(contribeUser);

                        Activity activity = new Activity();
                        activity.setCreatedDate(new Date());
                        activity.setActivity("Create Loan for Approval");
                        activity.setPerformedBy(user.getIppisNumber() + " - "
                                + user.getName());
                        activity.setPerformedOn(contribeUser.getIppisNumber()
                                + " - " + contribeUser.getName());
                        activity.setPhoneNumber(user.getPhoneNumber());
                        activity.setRole(user.getRole().name());
                        entityManager.persist(activity);

                        return ResponseUtil.success("Loan created successfully, "
                                + "wait for approval.", null);
                    }
                } else {
                    return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                            "User does not have permission to perform operation", null);
                }
            }

        }
    }

    public BaseResponse remitLoan(User user, String ippis, Double amount,
            Long loanID) {

        if (Objects.isNull(ippis) || Objects.isNull(amount)
                || (amount < 1)) {
            return ResponseUtil.badInputParam("User not specified and or invalid amount.");
        } else {
            if (user.getRole() == User.Role.ADMIN_1) {

                List<User> users = entityManager
                        .createNamedQuery("User.findByIppisNumber", User.class)
                        .setParameter("number", ippis).getResultList();
                if (users.isEmpty()) {
                    return ResponseUtil.recordNotFound("Invalide ippis number");
                } else {
                    User contribeUser = users.get(0);

                    Loan loan = entityManager.find(Loan.class, loanID);

                    if (Objects.isNull(loan)) {
                        return ResponseUtil.recordNotFound("Loan not found");
                    } else {
                        if (null == loan.getStatus()) {

                            loan.setPaid(loan.getPaid() + amount);
                            if (loan.getPaid() >= loan.getAmount()) {
                                loan.setStatus(Loan.Status.CLEARED);
                            } else {
                                loan.setStatus(Loan.Status.PARTIAL);
                            }

                            entityManager.merge(loan);

                            String sms = "Your Coop. Loan A/C with IPPIS " + loan.getUser().getIppisNumber()
                                    + " has been debited with N" + amount + " for the month of "
                                    + Util.getMonthFromDate(new Date()) + ", " + Util.getCurrentYear()
                                    + ". Oustanging Bal. N" + (loan.getAmount() - loan.getPaid())
                                    + " (Loan).";
                            smsManager.sendSms(sms, loan.getUser().getPhoneNumber());

                            Activity activity = new Activity();
                            activity.setCreatedDate(new Date());
                            activity.setActivity("Remitting " + amount
                                    + " from loan, with id: " + loan.getId());
                            activity.setPerformedBy(user.getIppisNumber() + " - "
                                    + user.getName());
                            activity.setPerformedOn(contribeUser.getIppisNumber()
                                    + " - " + contribeUser.getName());
                            activity.setPhoneNumber(user.getPhoneNumber());
                            activity.setRole(user.getRole().name());
                            entityManager.persist(activity);

                            return ResponseUtil.success("Money remited successfully",
                                    null);
                        } else {
                            switch (loan.getStatus()) {
                                case CLEARED:
                                    return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                            "Loan has been cleared", null);
                                case MERGED:
                                    return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                            "This loan has been merged to another loan", null);
                                default:
                                    loan.setPaid(loan.getPaid() + amount);
                                    if (loan.getPaid() >= loan.getAmount()) {
                                        loan.setStatus(Loan.Status.CLEARED);
                                    } else {
                                        loan.setStatus(Loan.Status.PARTIAL);
                                    }

                                    entityManager.merge(loan);

                                    String sms = "Your Coop. Loan A/C with IPPIS " + loan.getUser().getIppisNumber()
                                            + " has been debited with N" + amount + " for the month of "
                                            + Util.getMonthFromDate(new Date()) + ", " + Util.getCurrentYear()
                                            + ". Oustanging Bal. N" + (loan.getAmount() - loan.getPaid())
                                            + " (Loan).";
                                    smsManager.sendSms(sms, loan.getUser().getPhoneNumber());

                                    Activity activity = new Activity();
                                    activity.setCreatedDate(new Date());
                                    activity.setActivity("Remitting " + amount
                                            + " from loan, with id: " + loan.getId());
                                    activity.setPerformedBy(user.getIppisNumber() + " - "
                                            + user.getName());
                                    activity.setPerformedOn(contribeUser.getIppisNumber()
                                            + " - " + contribeUser.getName());
                                    activity.setPhoneNumber(user.getPhoneNumber());
                                    activity.setRole(user.getRole().name());
                                    entityManager.persist(activity);

                                    return ResponseUtil.success("Money remited successfully",
                                            null);
                            }
                        }
                    }

                }
            } else {
                return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                        "User does not have permission to perform operation", null);
            }
        }
    }

    public BaseResponse getAwaitingLoans(User user, Long lastId) {
        if (Objects.isNull(user)) {
            return ResponseUtil.badInputParam("User not logged in");
        } else {
            if (Objects.isNull(lastId) || (lastId < 1)) {
                lastId = Long.MAX_VALUE;
            }

            List<Loan> loans = entityManager
                    .createQuery("SELECT l FROM Loan l WHERE l.approval = 'PENDING' "
                            + "AND l.id <= :id ORDER BY l.id DESC",
                            Loan.class).setParameter("id", lastId)
                    .setMaxResults(Constant.QUERY_SIZE)
                    .getResultList();
            return ResponseUtil.success("Success", loans);
        }
    }

    public BaseResponse getAllActiveLoans(User user, Long lastId) {
        if (Objects.isNull(user)) {
            return ResponseUtil.badInputParam("User not logged in");
        } else {
            if (Objects.isNull(lastId) || (lastId < 1)) {
                lastId = Long.MAX_VALUE;
            }

            List<Loan.Status> statuses = Arrays.asList(Loan.Status.PARTIAL,
                    Loan.Status.UNCLEAR);

            List<Loan> loans = entityManager.createNamedQuery("Loan.findByStatuses",
                    Loan.class).setParameter("id", lastId).setParameter("status",
                    statuses).setMaxResults(Constant.QUERY_SIZE)
                    .getResultList();
            return ResponseUtil.success("Success", loans);
        }
    }

    public BaseResponse getAllUserActiveLoans(String ippis) {
        if (Objects.isNull(ippis)) {
            return ResponseUtil.badInputParam("Ippis not supplied");
        } else {
            Long lastId = null;
            if (Objects.isNull(lastId) || (lastId < 1)) {
                lastId = Long.MAX_VALUE;
            }

            List<Loan.Status> statuses = Arrays.asList(Loan.Status.PARTIAL,
                    Loan.Status.UNCLEAR);

            List<Loan> loans = entityManager.createNamedQuery("Loan.findByUserAndStatuses",
                    Loan.class).setParameter("id", lastId)
                    .setParameter("status", statuses)
                    .setParameter("ippis", ippis)
                    .setMaxResults(Constant.QUERY_SIZE)
                    .getResultList();
            return ResponseUtil.success("Success", loans);
        }
    }

    public BaseResponse getUserApprovedLoans(User user, Long id) {
        if (Objects.isNull(user)) {
            return ResponseUtil.notLoggedIn();
        } else {
            if (Objects.isNull(id) || (id < 1)) {
                id = Long.MAX_VALUE;
            }
            String ippis = user.getIppisNumber();
            List<Loan> loans = entityManager.createQuery("SELECT l FROM Loan l WHERE "
                    + "l.approval = :approval AND l.user.ippisNumber = :ippis AND "
                    + "l.id <= :id ORDER BY l.id DESC", Loan.class)
                    .setParameter("approval", Loan.Approval.APPROVED)
                    .setParameter("ippis", ippis).setParameter("id", id)
                    .setMaxResults(Constant.QUERY_SIZE).getResultList();
            return ResponseUtil.success("Successs", loans);
        }
    }

    public BaseResponse getUserApprovedLoansByType(User user, Long id, Loan.Type type) {
        if (Objects.isNull(user)) {
            return ResponseUtil.notLoggedIn();
        } else {

            if (Objects.isNull(type)) {
                type = Loan.Type.REGULAR;
            }

            if (Objects.isNull(id) || (id < 1)) {
                id = Long.MAX_VALUE;
            }
            String ippis = user.getIppisNumber();
            List<Loan> loans = entityManager.createQuery("SELECT l FROM Loan l WHERE "
                    + "l.approval = :approval AND l.user.ippisNumber = :ippis AND "
                    + "l.id <= :id AND l.type = :type ORDER BY l.id DESC", Loan.class)
                    .setParameter("approval", Loan.Approval.APPROVED)
                    .setParameter("ippis", ippis)
                    .setParameter("type", type)
                    .setParameter("id", id)
                    .setMaxResults(Constant.QUERY_SIZE).getResultList();
            return ResponseUtil.success("Successs", loans);
        }
    }

    public BaseResponse approveLoan(User user, Long id, Loan.Approval approval) {
        if (Objects.isNull(user)) {
            return ResponseUtil.notLoggedIn();
        } else {
            if (Objects.isNull(id) || (id < 1) || Objects.isNull(approval)) {
                return ResponseUtil.badInputParam("Invalid loan id or approval");
            } else {
                Loan loan = entityManager.find(Loan.class, id);
                if (Objects.isNull(loan)) {
                    return ResponseUtil.badInputParam("Invalid loan id");
                } else {
                    if (loan.getApproval() == Loan.Approval.PENDING) {
                        if (approval == Loan.Approval.APPROVED) {
                            loan.setApproval(approval);
                            entityManager.merge(loan);

                            Activity activity = new Activity();
                            activity.setCreatedDate(new Date());
                            activity.setActivity("Loan with id = " + id
                                    + " appproved");
                            activity.setPerformedBy(user.getIppisNumber() + " - "
                                    + user.getName());
                            activity.setPerformedOn(loan.getUser().getIppisNumber()
                                    + " - " + loan.getUser().getName());
                            activity.setPhoneNumber(user.getPhoneNumber());
                            activity.setRole(user.getRole().name());
                            entityManager.persist(activity);

                            smsManager.sendSms("Your Coop. Loan for IPPIS "
                                    + loan.getUser().getIppisNumber() + "has been approved for you, "
                                    + "Cash ready for pick up."
                                    + "Thanks NGA", loan.getUser().getPhoneNumber());

                            return ResponseUtil.success("Loan approved successfully", null);
                        } else if (approval == Loan.Approval.DECLINED) {
                            loan.setApproval(approval);
                            entityManager.merge(loan);

                            Activity activity = new Activity();
                            activity.setCreatedDate(new Date());
                            activity.setActivity("Loan with id = " + id
                                    + " declined");
                            activity.setPerformedBy(user.getIppisNumber() + " - "
                                    + user.getName());
                            activity.setPerformedOn(loan.getUser().getIppisNumber()
                                    + " - " + loan.getUser().getName());
                            activity.setPhoneNumber(user.getPhoneNumber());
                            activity.setRole(user.getRole().name());
                            entityManager.persist(activity);

                            smsManager.sendSms("Loan declined, "
                                    + "Please try again in a few days."
                                    + "Thanks NGA", loan.getUser().getPhoneNumber());

                            return ResponseUtil.success("Loan declined successfully", null);
                        } else {
                            return ResponseUtil.badInputParam("Approval status not allowed");
                        }
                    } else {
                        return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                "Loan has been acted on earlier", null);
                    }
                }
            }
        }
    }

    public BaseResponse updateLoanRepayment(RepaymentBean repaymentBean) {
        if (Objects.isNull(repaymentBean) || Objects.isNull(repaymentBean.getId())
                || Objects.isNull(repaymentBean.getRepayment())) {
            return ResponseUtil.badInputParam("Invalid input parameters");
        } else {
            if (repaymentBean.getRepayment() < 1) {
                return ResponseUtil.badInputParam("Repayment amount is invalid");
            } else {
                Loan loan = entityManager.find(Loan.class, repaymentBean.getId());
                if (Objects.isNull(loan)) {
                    return ResponseUtil.recordNotFound("Loan not found");
                } else {
                    loan.setRepayment(repaymentBean.getRepayment());
                    entityManager.merge(loan);
                    return ResponseUtil.success("Loan updated successfully", null);
                }
            }
        }
    }

    public BaseResponse getRepayments(Long loanId) {
        if (Objects.isNull(loanId) || (loanId < 1)) {
            return ResponseUtil.badInputParam("Invalid input parameters");
        } else {
                Loan loan = entityManager.find(Loan.class, loanId);
                if (Objects.isNull(loan)) {
                    return ResponseUtil.recordNotFound("Loan not found");
                } else {
                  Set<LoanPaymentHistory> loanPayments = loan.getLoanPayments();
                    return ResponseUtil.success("Loan updated successfully", loanPayments);
                }
        }
    }

    public BaseResponse getSavings(User user, Long lastID, String year) {

        if (Objects.isNull(user)) {
            return ResponseUtil.notLoggedIn();
        }

        if (Objects.isNull(lastID) || (lastID <= 0)) {
            lastID = Long.MAX_VALUE;
        }

        if (Objects.isNull(year)) {
            year = Year.now().toString();
        }

        List<Saving> savings = entityManager.createNamedQuery("Saving.findByIppisAndYear",
                Saving.class).setParameter("ippis", user.getIppisNumber())
                .setParameter("id", lastID)
                .setParameter("year", Integer.parseInt(year))
                .setMaxResults(Constant.QUERY_SIZE)
                .getResultList();
        return ResponseUtil.success("Success", savings);
    }

    public BaseResponse getSavings(User user, String ippis, Long lastID, String year) {

        if (Objects.isNull(user)) {
            return ResponseUtil.notLoggedIn();
        }

        if (Objects.isNull(lastID) || (lastID <= 0)) {
            lastID = Long.MAX_VALUE;
        }

        if (Objects.isNull(year)) {
            year = Year.now().toString();
        }

        List<Saving> savings = entityManager.createNamedQuery("Saving.findByIppisAndYear",
                Saving.class).setParameter("ippis", ippis)
                .setParameter("id", lastID)
                .setParameter("year", Integer.parseInt(year))
                .setMaxResults(Constant.QUERY_SIZE)
                .getResultList();
        return ResponseUtil.success("Success", savings);
    }

    public BaseResponse getWithdrawals(User user, Long lastID, String year) {
        if (Objects.isNull(lastID) || (lastID <= 0)) {
            lastID = Long.MAX_VALUE;
        }

        if (Objects.isNull(year)) {
            year = Year.now().toString();
        }

        List<Withdrawal> withdrawals = entityManager
                .createNamedQuery("Withdrawal.findByIppisAndYear", Withdrawal.class)
                .setParameter("ippis", user.getIppisNumber())
                .setParameter("status", Withdrawal.Status.APPROVED)
                .setParameter("year", Integer.parseInt(year))
                .setParameter("id", lastID)
                .setMaxResults(Constant.QUERY_SIZE)
                .getResultList();

        return ResponseUtil.success("Success", withdrawals);
    }

    public BaseResponse getWithdrawals(User user, String ippis, Long lastID, String year) {
        if (Objects.isNull(lastID) || (lastID <= 0)) {
            lastID = Long.MAX_VALUE;
        }

        if (Objects.isNull(year)) {
            year = Year.now().toString();
        }

        List<Withdrawal> withdrawals = entityManager
                .createNamedQuery("Withdrawal.findByIppisAndYear", Withdrawal.class)
                .setParameter("ippis", ippis)
                .setParameter("status", Withdrawal.Status.APPROVED)
                .setParameter("year", Integer.parseInt(year))
                .setParameter("id", lastID)
                .setMaxResults(Constant.QUERY_SIZE)
                .getResultList();

        return ResponseUtil.success("Success", withdrawals);
    }

    public BaseResponse createWithdrawal(User user, String ippis, Double amount) {

        if (Objects.isNull(ippis) || Objects.isNull(amount)
                || (amount < 1)) {
            return ResponseUtil.badInputParam("User and or amount not specify");
        } else {
            if (user.getRole() == User.Role.ADMIN_1) {

                List<User> users = entityManager
                        .createNamedQuery("User.findByIppisNumber", User.class)
                        .setParameter("number", ippis).getResultList();
                if (users.isEmpty()) {
                    return ResponseUtil.recordNotFound("Invalide ippis number");
                } else {
                    User contribeUser = users.get(0);

                    Withdrawal withdrawal = new Withdrawal();
                    withdrawal.setAmount(amount);
                    withdrawal.setCreatedDate(new Date());
                    withdrawal.setRemarks("Personal withdrawal request");
                    withdrawal.setStatus(Withdrawal.Status.PENDING);
                    withdrawal.setUser(contribeUser);

                    entityManager.merge(withdrawal);
                    Activity activity = new Activity();
                    activity.setCreatedDate(new Date());
                    activity.setActivity("Request for withdrawal");
                    activity.setPerformedBy(user.getIppisNumber() + " - "
                            + user.getName());
                    activity.setPerformedOn(contribeUser.getIppisNumber()
                            + " - " + contribeUser.getName());
                    activity.setPhoneNumber(user.getPhoneNumber());
                    activity.setRole(user.getRole().name());
                    entityManager.persist(activity);

                    return ResponseUtil.success("request for withdrawal successful", null);
                }

            } else {
                return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                        "User does not have permission to perform operation", null);
            }
        }
    }

    public BaseResponse approveWithdrawal(User user, Long id, Withdrawal.Status status) {
        if (Objects.isNull(user) || Objects.isNull(id) || (id < 1)) {
            return ResponseUtil.badInputParam("user not login or bad transaction id");
        } else {
            if (user.getRole() == User.Role.ADMIN_2) {
                Withdrawal withdrawal = entityManager.find(Withdrawal.class, id);
                if (Objects.isNull(withdrawal)) {
                    return ResponseUtil.badInputParam("Transaction not found");
                } else {
                    if (withdrawal.getStatus() == Withdrawal.Status.APPROVED) {
                        return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                "Withdrawal has been approved already", null);
                    } else if (withdrawal.getStatus() == Withdrawal.Status.DECLINED) {
                        return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                "Withdrawal has been declined", null);
                    } else {
                        String ippisNumber = withdrawal.getUser().getIppisNumber();
                        List<User> users = entityManager
                                .createNamedQuery("User.findByIppisNumber", User.class)
                                .setParameter("number", ippisNumber).getResultList();
                        if (users.isEmpty()) {
                            return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                    "Error occured, Account not found", null);
                        } else {
                            if (status == Withdrawal.Status.APPROVED) {
                                User contribeUser = users.get(0);
                                List<Balance> balances = entityManager
                                        .createNamedQuery("Balance.currentYear", Balance.class)
                                        .setParameter("ippis", ippisNumber).getResultList();
                                Balance balance = null;

                                if (balances.isEmpty()) {
                                    balance = new Balance();
                                    balance.setBalanceBefore(contribeUser.getBalanceBefore());
                                    balance.setClosingBalance(0.0);
                                    balance.setUser(contribeUser);
                                } else {
                                    balance = balances.get(0);
                                }

                                withdrawal.setStatus(status);
                                withdrawal.setBalance(balance.getCurrentBalance()
                                        - withdrawal.getAmount());
                                balance.setCurrentBalance(balance.getCurrentBalance()
                                        - withdrawal.getAmount());
                                entityManager.merge(withdrawal);
                                entityManager.merge(balance);

                                Activity activity = new Activity();
                                activity.setCreatedDate(new Date());
                                activity.setActivity("Withdrawal with id = " + id
                                        + " appproved");
                                activity.setPerformedBy(user.getIppisNumber() + " - "
                                        + user.getName());
                                activity.setPerformedOn(contribeUser.getIppisNumber()
                                        + " - " + contribeUser.getName());
                                activity.setPhoneNumber(user.getPhoneNumber());
                                activity.setRole(user.getRole().name());
                                entityManager.persist(activity);

                                smsManager.sendSms("Withdraw approved to you, please "
                                        + "pick your cash up."
                                        + "Thanks NGA", contribeUser.getPhoneNumber());

                                return ResponseUtil
                                        .success("Transaction approved successfully", null);
                            } else {
                                User contribeUser = users.get(0);
                                withdrawal.setStatus(status);
                                entityManager.merge(withdrawal);

                                Activity activity = new Activity();
                                activity.setCreatedDate(new Date());
                                activity.setActivity("Withdrawal with id = " + id
                                        + " declined");
                                activity.setPerformedBy(user.getIppisNumber() + " - "
                                        + user.getName());
                                activity.setPerformedOn(contribeUser.getIppisNumber()
                                        + " - " + contribeUser.getName());
                                activity.setPhoneNumber(user.getPhoneNumber());
                                activity.setRole(user.getRole().name());
                                entityManager.persist(activity);

                                smsManager.sendSms("Withdraw delined, please "
                                        + "try again in a few days."
                                        + "Thanks NGA", contribeUser.getPhoneNumber());

                                return ResponseUtil.respond(ResponseCodes.PARTIAL_SUCCESS,
                                        "Transaction declined", null);
                            }
                        }
                    }
                }
            } else {
                return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                        "User does not have permisssion to perform operation", null);
            }
        }
    }

    public BaseResponse getAwaitingWithdrawal(User user, Long lastID) {
        if (Objects.isNull(user)) {
            return ResponseUtil.badInputParam("User not logged in");
        } else {

            if (Objects.isNull(lastID) || (lastID < 1)) {
                lastID = Long.MAX_VALUE;
            }
            List<Withdrawal> withdrawals = entityManager
                    .createNamedQuery("Withdrawal.findAllByStatus", Withdrawal.class)
                    .setParameter("status", Withdrawal.Status.PENDING)
                    .setParameter("id", lastID).setMaxResults(Constant.QUERY_SIZE)
                    .getResultList();
            return ResponseUtil.success("Success", withdrawals);
        }
    }

    public BaseResponse getUsers(User user, Long lastID) {
        if (Objects.isNull(lastID) || (lastID <= 0)) {
            lastID = Long.MAX_VALUE;
        }
        List<User> users = entityManager.createNamedQuery("User.findAll", User.class)
                .setParameter("id", lastID).setMaxResults(Constant.QUERY_SIZE)
                .getResultList();
        return ResponseUtil.success("Success", users);
    }

    public BaseResponse searchUsers(User user, String ippis) {
        List<User> users = entityManager.createNamedQuery("User.searchForUser",
                User.class).setParameter("number", ippis).getResultList();
        return ResponseUtil.success("Success", users);
    }

    public BaseResponse deleteUser(User user, String ippis) {

        if (user.getRole() == User.Role.NORMAL) {
            return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                    "User does not have permission", null);
        } else {
            List<User> users = entityManager.createNamedQuery("User.findByIppisNumber",
                    User.class).setParameter("number", ippis)
                    .getResultList();
            if (users.isEmpty()) {
                return ResponseUtil.recordNotFound("Invalid ippis number");
            } else {
                User contribUser = users.get(0);
                contribUser.getBalances().clear();
                for (Saving saving : contribUser.getSavings()) {
                    saving.setUser(null);
                    entityManager.remove(saving);
                }
                for (Balance balance : contribUser.getBalances()) {
                    balance.setUser(null);

                }
                for (Withdrawal withdrawal : contribUser.getWithdrawals()) {
                    withdrawal.setUser(null);
                }
                contribUser.getSavings().clear();
                contribUser.getBalances().clear();
                contribUser.getWithdrawals().clear();
                entityManager.remove(contribUser);

                Activity activity = new Activity();
                activity.setCreatedDate(new Date());
                activity.setActivity("Account Delete");
                activity.setPerformedBy(user.getIppisNumber() + " - " + user.getName());
                activity.setPerformedOn(contribUser.getIppisNumber()
                        + " - " + contribUser.getName());
                activity.setPhoneNumber(user.getPhoneNumber());
                activity.setRole(user.getRole().name());
                entityManager.persist(activity);

                return ResponseUtil.success("User deleted successful", null);
            }
        }
    }

    public BaseResponse updateInfo(User user, UserBean userBean) {

        List<User> users = entityManager.createNamedQuery("User.findByIppisNumber",
                User.class).setParameter("number", userBean.getIppis())
                .getResultList();
        if (users.isEmpty()) {
            return ResponseUtil.recordNotFound("Invalid ippis number");
        } else {
            User contribeUser = users.get(0);

            contribeUser.setEmail(Objects.isNull(userBean.getEmail()) ? contribeUser
                    .getEmail() : userBean.getEmail());
            contribeUser.setName(Objects.isNull(userBean.getName()) ? contribeUser
                    .getName() : userBean.getName());
            contribeUser.setPhoneNumber(Objects.isNull(userBean.getPhone()) ? contribeUser
                    .getPhoneNumber() : userBean.getPhone());
            contribeUser.setContributionAmount(Objects.isNull(userBean.getAmount())
                    ? contribeUser.getContributionAmount() : userBean.getAmount());
            contribeUser.setRole(Objects.isNull(userBean.getRole())
                    ? contribeUser.getRole() : userBean.getRole());

            if (Objects.nonNull(userBean.getRole())) {

                Activity activity = new Activity();
                activity.setCreatedDate(new Date());
                activity.setActivity("Asign Permission");
                activity.setPerformedBy(user.getIppisNumber() + " - " + user.getName());
                activity.setPerformedOn(contribeUser.getIppisNumber()
                        + " - " + contribeUser.getName());
                activity.setPhoneNumber(user.getPhoneNumber());
                activity.setRole(user.getRole().name());
                entityManager.persist(activity);
            } else {

                Activity activity = new Activity();
                activity.setCreatedDate(new Date());
                activity.setActivity("Update Information");
                activity.setPerformedBy(user.getIppisNumber() + " - " + user.getName());
                activity.setPerformedOn(contribeUser.getIppisNumber()
                        + " - " + contribeUser.getName());
                activity.setPhoneNumber(user.getPhoneNumber());
                activity.setRole(user.getRole().name());
                entityManager.persist(activity);
            }
            
            if(Objects.nonNull(userBean.getPassword())){
                String password = userBean.getPassword();
                String cPassword = userBean.getCpassword();
                if(password.equals(cPassword)){
                    String hashPasword = passwordManager.hashPasword(password);
                    contribeUser.setPassword(hashPasword);
                    entityManager.merge(contribeUser);
                }else{
                    return ResponseUtil.respond(ResponseCodes.INPUT_ERROR, 
                            "Password does not match", null);
                }
            }

            entityManager.merge(contribeUser);
            return ResponseUtil.success("User information updated", null);
        }
    }

    public BaseResponse getAdmin1Activities(User user, Long lastID) {
        if (user.getRole() == User.Role.ADMIN_2) {

            if (Objects.isNull(lastID) || (lastID < 0)) {
                lastID = Long.MAX_VALUE;
            }

            List<Activity> activities = entityManager
                    .createNamedQuery("Activity.findForAdmin1", Activity.class)
                    .setParameter("id", lastID)
                    .setParameter("role", "ADMIN_1").setMaxResults(Constant.QUERY_SIZE)
                    .getResultList();
            return ResponseUtil.success("Success", activities);
        } else {
            return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                    "User does not have permission", null);
        }
    }

    public BaseResponse getAllActivities(User user, Long lastID) {
        if (User.Role.ADMIN_3 == user.getRole()) {

            if (Objects.isNull(lastID) || (lastID < 0)) {
                lastID = Long.MAX_VALUE;
            }

            List<Activity> activities = entityManager
                    .createNamedQuery("Activity.findAll", Activity.class)
                    .setParameter("id", lastID).setMaxResults(Constant.QUERY_SIZE)
                    .getResultList();
            return ResponseUtil.success("Success", activities);
        } else {
            return ResponseUtil.respond(ResponseCodes.OPERATION_NOT_PERMITTED,
                    "User does not have permission", null);
        }
    }

    public BaseResponse getSummary(User user, int year) {

        Double totalSaving = entityManager
                .createQuery("SELECT SUM(s.amount) FROM Saving s WHERE "
                        + "s.user.ippisNumber = :ippis AND YEAR(s.createdDate) = :year",
                        Double.class).setParameter("year", year)
                .setParameter("ippis", user.getIppisNumber()).getResultList().get(0);
        Double totalWithdrawal = entityManager
                .createQuery("SELECT SUM(w.amount) FROM Withdrawal w WHERE "
                        + "w.user.ippisNumber = :ippis AND YEAR(w.createdDate) = :year "
                        + "AND w.status = :status",
                        Double.class).setParameter("year", year)
                .setParameter("status", Withdrawal.Status.APPROVED)
                .setParameter("ippis", user.getIppisNumber()).getResultList().get(0);

        List<Balance> balances = entityManager.createNamedQuery("Balance.findByYear",
                Balance.class).setParameter("ippis", user.getIppisNumber())
                .setParameter("year", String.valueOf(year)).getResultList();
        Balance balance = null;
        if (balances.isEmpty()) {
            balance = new Balance(0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, String.valueOf(year), null);
        } else {
            balance = balances.get(0);
        }

        Double totalLoan = entityManager.createQuery("SELECT SUM(l.amount) FROM Loan l "
                + "WHERE l.user.ippisNumber = :ippis AND YEAR(l.createdDate) = :year AND "
                + "l.approval = :approval",
                Double.class)
                .setParameter("ippis", user.getIppisNumber())
                .setParameter("year", year)
                .setParameter("approval", Loan.Approval.APPROVED)
                .getResultList().get(0);

        if (Objects.isNull(totalSaving)) {
            totalSaving = 0.0;
        }

        if (Objects.isNull(totalWithdrawal)) {
            totalWithdrawal = 0.0;
        }

        if (Objects.isNull(totalLoan)) {
            totalLoan = 0.0;
        }

        return ResponseUtil.success("Success",
                Arrays.asList(balance, totalSaving, totalWithdrawal, totalLoan));
    }

    public BaseResponse downloadUsers(User user) throws IOException {
        List<ExportData> exportDatas = new ArrayList<>();
        List<User> users = entityManager
                .createQuery("SELECT u FROM User u ORDER BY u.id DESC", User.class).getResultList();
        for (User u : users) {
            double loanRepayment = 0.0;
            double saving = 0.0;
            List<Loan> loans = entityManager.createNamedQuery("Loan.findByNotStatus", Loan.class)
                    .setParameter("status", Loan.Status.CLEARED)
                    .setParameter("ippis", u.getIppisNumber())
                    .getResultList();
            if (!loans.isEmpty()) {
                Loan loan = loans.get(0);
                loanRepayment = loan.getRepayment();
            }

            try {
                Balance balance = entityManager.createNamedQuery("Balance.currentYear",
                        Balance.class).setParameter("ippis", u.getIppisNumber())
                        .setMaxResults(1)
                        .getSingleResult();
                saving = balance.getCurrentBalance();
            } catch (NoResultException nre) {

            }

            ExportData exportData = new ExportData();
            exportData.setIppis(u.getIppisNumber());
            exportData.setName(u.getName());
            exportData.setMonthlySaving(u.getContributionAmount());
            exportData.setMonthlyLoanDeduction(loanRepayment);
            exportData.setSaving(saving);
            exportDatas.add(exportData);
        }
        String filePath = Csv.toFile(exportDatas);

        MessageLogger.info(Service.class, filePath);
        return ResponseUtil.success("Sucess", filePath);
    }

    public BaseResponse downloadUsers(User user, String month) throws IOException {
        List<ExportData> exportDatas = new ArrayList<>();
        List<User> users = entityManager
                .createQuery("SELECT u FROM User u ORDER BY u.id DESC", User.class).getResultList();
        for (User u : users) {
            double loanRepayment = 0.0;
            double saving = 0.0;

            List<Loan> loans = entityManager.createNamedQuery("Loan.findByIppisAndMonth", Loan.class)
                    .setParameter("month", month)
                    .setParameter("ippis", u.getIppisNumber())
                    .setMaxResults(1)
                    .getResultList();
            if (!loans.isEmpty()) {
                Loan loan = loans.get(0);
                loanRepayment = loan.getRepayment();
            }

            List<Saving> savings = entityManager.createNamedQuery("Saving.findByIppisAndMonth", Saving.class)
                    .setParameter("ippis", u.getIppisNumber())
                    .setParameter("month", month)
                    .setMaxResults(1)
                    .getResultList();
            if (!savings.isEmpty()) {
                Saving sav = savings.get(0);
                saving = sav.getBalance();
            }

            ExportData exportData = new ExportData();
            exportData.setIppis(u.getIppisNumber());
            exportData.setName(u.getName());
            exportData.setMonthlySaving(u.getContributionAmount());
            exportData.setMonthlyLoanDeduction(loanRepayment);
            exportData.setSaving(saving);
            exportDatas.add(exportData);
        }
        String filePath = Csv.toFile(exportDatas);
        return ResponseUtil.success("Sucess", filePath);
    }

}
